#!/bin/bash
#
# Build cheatsheets
#
# Author:
#   Tom Schraitle
#   March, 2020

# Script name without any directories ("basename")
PROG=${0##*/}

# output directory:
OUTPUTDIR="pdf"

# Global matching pattern for TeX cheatsheet files
PATTERN='*-cheatsheet.tex'

# Available languages:
PDF_LANGUAGES="${PDF_LANGUAGES:-de en}"

RESULT=0

# -- Color codes
RED="\e[1;31m"
# VIOLET="\e[35m"
# BLUE="\e[94m"
YELLOW="\e[93m"
CYAN="\e[36m"
# GRAY="\e[37m"

BOLD="\e[1m"
# NORMAL=""
# REVERSE="\e[8m"
RESET="\e[0m"

# -- Verbosity level:
VERBOSITY=0
LOGGING_LEVEL="DEBUG"
declare -A LOGLEVELS=([DEBUG]=0 [INFO]=1 [WARN]=2 [ERROR]=3)
declare -A LEVEL2LOG=([0]="ERROR" [1]="WARN" [2]="INFO" [3]="DEBUG")
declare -A LOGCOLORS=([DEBUG]=$CYAN [INFO]="$BOLD$CYAN" [WARN]=$YELLOW [ERROR]=$RED)

# Version information:
VERSION="1.0.0"

# Arguments (usually TeX files):
ARGS=""

# Export the "sty" directory:
export TEXINPUTS="$PWD/sty:"


# -- Logging Functions
function logger() {
    # Log message with a certain log level
    #
    # Arguments
    #  $1 = log level ("DEBUG", "INFO", "WARN" or "ERROR")
    #  $2 = message to log
    #
    local log_priority="$1"
    local msg="$2"
    local color

    #check if level exists
    [[ ${LOGLEVELS[$log_priority]} ]] || return 1

    #check if level is enough
    (( ${LOGLEVELS[$log_priority]} < ${LOGLEVELS[$LOGGING_LEVEL]} )) && return 2

    color=${LOGCOLORS[$log_priority]}

    #log here
    echo -e "${color}${log_priority}:${RESET} ${msg}"
}

function logdebug {
    logger "DEBUG" "$1"
}

function loginfo {
    logger "INFO" "$1"
}

function logwarn {
    logger "WARN" "$1"
}

function logerror {
    logger "ERROR" "$1"
}

function exit_on_error {
    # Log error and exit
    #
    # Arguments
    #  $1 = log message
    #  $2 = return code, default 1
    #
    local err=${2:=1}
    logerror "$1" >&2
    exit $err;
}

trap "exit_on_error '\nCaught SIGTERM/SIGINT'" SIGTERM SIGINT

# -- Build Functions
function build_pdf_from_tex() {
    # Create PDF from one TeX file
    #
    # Parameters:
    #  $1 = .tex file with directory part
    local tex=$1
    local dir="${1%/*}/$OUTPUTDIR"
    pdflatex -recorder -interaction=scrollmode -output-directory=$dir $tex
}


function build_all_pdfs() {
    # Build all PDFs from different languages
    #
    # Parameters:
    #  $1 = Available languages, separated by space
    #  $2 = Optional TeX files
    local l
    local LANGS="$1"
    local TEXFILES="$2"
    logdebug "build_all_pdfs"
    logdebug "  LANG=$LANGS"
    logdebug "  TEXFILES=$TEXFILES"
    local ALL=""
    for l in $LANGS; do
       logdebug "lang=$l"
       ALL="$l/$PATTERN $ALL"
       mkdir -p $l/$OUTPUTDIR 2>/dev/null
    done
    local TEXFILES="${TEXFILES:=$ALL}"

    logdebug "TEXFILES=$TEXFILES"

    for tex in $TEXFILES; do
        loginfo "*** Building $tex first run ***"
        build_pdf_from_tex $tex
        RESULT=$?
        [[ -z $RESULT ]] && exit_on_error "problem when building $tex" $RESULT

        loginfo "*** Building $tex second run ***"
        build_pdf_from_tex $tex
        RESULT=$?
    done
}


function checks_args() {
    # Checks the positional arguments if they contain a directory part
    # Exit if not
    #
    local d
    local b
    logdebug "Check arguments: $ARGS"
    for tex in $ARGS; do
        d="${tex%/*}"
        b="${tex#*/}"
        if [ "$d" = "$b" ] || [ "$d" = "." ]; then
            exit_on_error "File $tex does not have a directory!"
        fi
    done
}


function usage() {
  cat << EOF
SYNOPSIS
   $PROG [-v]... [TEXFILE]...
   $PROG --version
   $PROG -h|--help

OPTIONS
   -v, -vv, -vvv..   Raise verbosity level
   -h, --help        Display this help and exit
   --version         Output version information and exit

ARGUMENTS
   TEXFILE           One or more .tex files with a directory path,
                     for example, 'de/foo.tex'
EOF
}


function parsecli() {
    # Parse command line
    #
    # Arguments
    #  $0 = Program name
    #  $@ = rest of the arguments
    #
    # For details about getopt,
    # see https://www.tutorialspoint.com/unix_commands/getopt.htm
    local options=$(getopt -n "$PROG" -o vh --long help,version -- "$@")
    [ $? -eq 0 ] || {
        exit_on_error "Incorrect option provided"
    }
    eval set -- "$options"
    while true; do
        case "$1" in
        -v)
            VERBOSITY=$(($VERBOSITY + 1))
            ;;
        -h|--help)
            usage
            exit
            ;;
        --version)
            echo "$PROG $VERSION"
            exit
            ;;
        --)
            shift
            break
            ;;
        esac
        shift
    done
    # Remove script name:
    shift
    ARGS="$@"

    # Fall back to 3 (=DEBUG) if we got more than 4
    [[ $VERBOSITY -ge 3 ]] && VERBOSITY=3
    LOGGING_LEVEL=${LEVEL2LOG[$VERBOSITY]}
}

# -- main
logdebug "Check requirements..."

parsecli $0 "$@"
checks_args

build_all_pdfs "$PDF_LANGUAGES" "$ARGS"

loginfo "Finished."
exit 0
