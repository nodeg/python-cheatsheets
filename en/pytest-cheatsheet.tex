\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

%opening
\title{Python3 Cheatsheet}
% \titlehead{fooo}
\subtitle{Writing Tests with \cmd{pytest} and \pyth{assert}}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
% multicol parameters
% These lengths are set only within the two main columns
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}
\begin{abstract}
% \noindent\gquote{Im Grunde genommen geht es beim Testen darum, Risiken zu
%        reduzieren.} --- \url{usersnap.com}\cite{usersnap.com}

\gquote{Program testing can be used to show the presence of bugs, but never
        show their absence!}\\\hfill -- Edsger W. Dijkstra
\end{abstract}

\section{Definition}
Accoding to Pol, Koomen and Spillner:

\begin{quote}
\gquote{Testing is the process of planning, preparation and measurement with
    the aim of determining the characteristics of an IT system and showing
    the difference between the actual and required state.}
\end{quote}

\section{Why Test?}
\label{sec.pytest.why}
Testing checks and evaluates software for its quality and the fulfillment
of its defined requirements.

A test cannot prove that there are no errors in the software (see above
quote from Dijkstra). However, testing is quite useful:

% https://towardsdatascience.com/2-reasons-why-developers-avoid-writing-tests-7e55515776e9

\begin{itemize}
\item Enables full freedom for changes without fear of introducing new errors.
\item Supports troubleshooting.
\item Enables to automate tests.
\item Helps with the introduction of new developers to the team.
\item Acts as a form of knowledge transfer.
\item Helps to detects \gquote{blind spots} in our code or makes it possible.
\item Increases the trustworthiness of a software.
% \item Hilft beim Sparen von Geld (nicht kurzfristig, eher langfristig).
\end{itemize}


\section{Terminology}
\begin{description}
\item[Assertion]
Condition within the test function whether the result obtained
matches the expected result.

\item[Exception]
Exceptional event that occurs during the execution of the program.
Causes can be invalid arguments, network errors, missing write permission, etc.

\item[Fixture]
Function that transfers data to a test function.

\item[Setup and Teardown Functions]
Create preconditions for a test suite to be executable.
For example the creation of a temporary database or the start of a server process.
Setup and teardown functions can be defined per test function, module or global.

\item[Test Case]
Single, individual unit of a test.

\item[Test Suite]
Set of tests to verify that a program shows a certain behavior.
\end{description}


\section{Types of Tests}
Based on the size of the objects to be tested, tests can be
can be distinguished (sorted from small to large):

\begin{description}
\item[Level 1: Unit Testing]
Checks individual functions, classes or modules, isolated
from the rest of the system.

\item[Level 2: Integration Testing]
Checks if single units are combined and tested as a group,
maybe several classes or a subsystem.
% For example, whether different modules or services can
% be tested with your application fit together.
Larger than a unit test, but smaller than a system test.

\item[Level 3: System Testing (\gquote{end-to-end})]
Verifies that the entire system under test \emph{as a whole}
meets the specified requirements.
This involves fitting the system as closely as possible
to the end user's environment.

\item[Level 4: Acceptance Testing]
Checks and evaluates a system for its suitability for the actual
requirements, needs or expectations of the customer.
% Es beurteilt, ob es die Abnahmekriterien erfüllt und
% für eine Auslieferung tauglich ist.
\end{description}

However, software testing can also be grouped by method:

\begin{description}
\item[Black-box Testing]
tests the software by focusing on the inputs and outputs
(the externally visible behavior).
The implementation is unimportant.
Example: Web page, which generates a certain representation
for a certain input.

\item[White-box Testing]
tests the software against its internal structure,
design or code. The implementation is important.
% The focus is mainly on the code
% or .\ the processing of input and output.
Example: a function containing several if-queries.
The developer needs to know which input is needed to process
all branches of the query in a test function.
\end{description}

\iffalse
Some of these terms are defined differently for each author or source.
There are also a number of other definitions that refer to
other aspects (stress testing, performance testing, etc).
However, they have no further meaning for this cheat sheet.
\fi

\section{Recommendations for tests and environment}
In order to use testing in a meaningful way, here are a few recommendations:

\begin{description}
\item[Write preferably small tests]

A test must be straightforward, i.e. it should contain few lines of code.
This is faster, better to understand, and easier to maintain.

\item[Focus on \emph{one} test functionality]

A test has exactly \emph{one} task (single responsibility).
However, you can test your function with more than one test functions.
Each test function tests a different aspect.

\iffalse
Eine Funktionalität ist eine Testfunktion. Wenn
du beispielsweise Addition und Subtraktion testen möchtest,
schreibe zwei separate Testfunktionen.
In Bezug auf OOP hat ein Test genau eine Aufgabe (single responsibility).
\fi
%
% Vermische keine Funktionalitäten, d.~h.\ teste in einer Funktion nicht
% beispielsweise Addition und Subtraktion sondern verwende verschiedene
% Testfunktionen für diese Aufgabe.

\item[Optimize your \gquote{test coverage}]

\emph{Test coverage} is a technique for determining how many lines of
your code have actually been executed by tests.
This allows you to identify areas of your code that still need testing.

% Er wird berechnet als ${T}/{A}*100$;
% $T$ bedeutet die Anzahl der ausgeführten Zeilen der Testsuite und
% $A$ die Gesamtzahl des zu testenden Codes.

A high value means that more of your code was executed during the test.
%
See plugin \module{pytest-cov} (Paket \filename{python3-pytest-cov});
\url{https://github.com/pytest-dev/pytest-cov}.
%
% Durch das Plugin \module{pytest-cov}\cite{pytest-cov} (Paket \filename{python3-pytest-cov})
% zeigt dir pytest an, welche Codezeilen durch einen Test noch nicht getestet
% wurden.

\item[Write independent test units]

Tests must never depend on other tests, the order is unimportant and
they can be executed alone or within the test suite without problems.

\item[Organize your tests like your source directory]

To make your tests easier to find, use the same structure and names
in the tests as in the source code.
For example, \filename{src/mypackage/foo/bar.py} has the test
\filename{tests/foo/test\_bar.py}.
% Das pytest-Framework wird alle finden solange du sie
% nach der üblichen Namenskonvention benennst (siehe Abschnitt~\ref{sec.names}).

\item[Write \gquote{fast} running tests]

The faster the better. A slow test blocks you during development.

% Eine Test-Suite die mehrere Stunden zum
% Testen brauchst, wirst du selten aufrufen. ;-)
%
Really slow tests should be marked, see section~\ref{sec.marker.usedefined}.

\item[Learn your test framework]

Self-explanatory, right? ;-)
% Damit du weißt, wie du dein Test-Framework erweitern kannst, wie du
% einzelnen Test startest oder die Fehlerausgabe zu lesen hast.

\item[Use descriptive names for your test functions]

Longer names for your test functions are ok, because they can be output
during testing and therefore give a better overview.

% In deinem normalen Quellcode sind lange Namen eher verpönt, aber
% als Testfunktionen ist es völlig ok. Der Grund ist, dass diese Namen
% beim Testen ausgegeben werden können und somit eine bessere Übersicht
% geben.
\end{description}


\section{Procedure for TDD}
How do you usually proceed? The following sequence is taken from \cite{wikipedia.tdd}:

\begin{enumerate}
\item \textbf{\sffamily Write a test}

Every new feature starts with a test first.
First think about what you want to test.
What does your feature expect, what does it return? Write a test about it.

\item \textbf{\sffamily Run the test (it must fail)}

The function to be tested is initially empty and will fail.

\item \textbf{\sffamily Write the code}

Write just enough code to run the test.
The code does not have to be perfect, it can even be inelegant or
be inefficient. What is important is that the test is successful.

\item \textbf{\sffamily Run all tests}

If all tests were successful, the new code meets all test criteria.
The new code neither worsens nor breaks other tests.
If it does, the new code must be adjusted until all tests are successful again.

\item \textbf{\sffamily Refactor your code}

The working code is now \gquote{optimized} regarding readability,
style, behavior, or even runtime.

\item \textbf{\sffamily Repeat}
\end{enumerate}

For practical reasons, the implementation \emph{before} the test may occur.
As long as a test exists for each feature/function, this is at the
developer's discretion.

\section{What is Pytest?}
\label{sec.pytest.definition}
\cmd{pytest}\cite{krekel.pytest} is a testing framework to write
tests with the \pyth{assert} keyword. The pytest framework
detects tests from \module{unittest} and \module{doctests}.


\section{Installing Pytest}
\begin{enumerate}
\item Execute the following command:
\begin{Verbatim}[commandchars=\\\{\}]
$ sudo zypper install python3-pytest
\end{Verbatim}

\item Check, if you have the correct version:
\begin{Verbatim}[commandchars=\\\{\}]
$ pytest --version
This is pytest version a.b.c, imported from
/usr/lib/python3.6/site-packages/pytest.py
\end{Verbatim}
\end{enumerate}

The pytest framework can be extended with plugins.
All packages are named by the convention \filename{python3-pytest-PLUGINNAME}.


\section{Configuring Pytest}
To save your configuration options, add them one of the following
files. The configuration file has to be located in the root folder
of your project. The files are read in this order:

\begin{itemize}
\item \filename{pytest.ini} (section \verb![pytest]!)
\item \filename{pyproject.toml}  (section \verb![tool.pytest.ini_options]!)
\item \filename{tox.ini}  (section \verb![pytest]!)
\item \filename{setup.cfg}  (section \verb![tool:pytest]!)
\end{itemize}

All files contain INI-like structures.
The following subsections give only a small insight.
Further options can be found under \cite{krekel.pytest.cmd.option}.

In the following examples, replace the placeholder \verb![ADD_PYTEST_SECTION]!
with the real section name from the previous list, depending on the file.

\subsection{Determining the Minimal Pytest Version}
Everything smaller than the given version will result in a
Error message:

\begin{inicode}
[ADD_PYTEST_SECTION]
minversion = 3.0  # will fail if we run with pytest-2.8
\end{inicode}


\subsection{Setting Default Options}
\label{sec.config.addopts}
With the key \option{addopts} you can pass default options to pytest.
For example, the \option{-v} (verbosity) option displays the
test ID (see section~\ref{sec.pytest.test-id}):

\begin{inicode}
[ADD_PYTEST_SECTION]
addopts = -v
\end{inicode}

This would be the same as calling \cmd{pytest -v}.


\subsection{Setting the Search Paths to Find Tests}
By default pytest searches all directories starting from
your project directory.
If no specific directories, files or test ID
(section~\ref{sec.pytest.test-id}) are specified on the
command line, the following setting may help:

\begin{inicode}
[ADD_PYTEST_SECTION]
testpaths = tests/  docs/
\end{inicode}

This will not detect unwanted tests that might be presen
 in a virtual Python environment.

\subsection{Excluding Search Paths}
Excludes certain paths during recursive searches.
This can be useful if you do not want to have your
virtual Python environment searched.
One or more values may be set:

\begin{inicode}
[ADD_PYTEST_SECTION]
norecursedirs = .git/ .venv/ tmp*/
\end{inicode}


\subsection{Changing the Naming Conventions}
\label{sec.names}
By default pytest searches for the following names:

\begin{itemize}
\item Test files: \filename{test\_*.py} oder \filename{*\_test.py}
\item Test classes: \cmd{Test*}
\item Test functions: \cmd{test\_*}
\end{itemize}

If you used a different naming convention for your test,
set the following values:

\begin{inicode}
[ADD_PYTEST_SECTION]
python_classes = Check
python_functions = *_check
python_files =
   check_*.py
   example_*.py
\end{inicode}

One or more values may be set. By the above configuration pytest
recognizes all files starting with \filename{check\_} or \filename{example\_}
and ending with \filename{.py}.

\iffalse
\section{Comparison of \module{unittest} vs. \module{pytest}}
Lege folgende Dateien an:
\begin{python}
# Inhalt der Datei project.py
def add(x, y):
    """Gibt zwei Zahlen oder Strings zurueck,
    die addiert/verkettet wurden
    """
    return x + y
\end{python}

\subsection{Example with \module{unittest}}
\cmd{unittest} ist ein Framework, ursprünglich von JUnit inspiriert.
\begin{python}
# Inhalt der Datei test_project_unittest.py
import unittest
from project import add

class TestAddMethod(unittest.TestCase):
    def test_numbers_1_2(self):
        """Testet, ob die Addition von zwei Ganzzahlen
        die korrekte Summe zurueckgibt
        """
        self.assertEqual(add(1, 2), 3)

    def test_strings_a_b(self):
        """Testet, ob die Addition von zwei Strings
        die beiden Strings als eine verkettete Zeichenfolge
        zurueckgibt
        """
        self.assertTrue(add("a", "b") == "ab")
        self.assertFalse(add("a", "b") == "abc")

if __name__ == '__main__':
    unittest.main()
\end{python}
\fi


\section{Selecting a Test Layout}
\label{sec.layout}
Mărieș recommends in \cite{maries.pypack} to separate the
source code (\filename{src/}) and tests directory (\filename{tests/})
from each other.
By this separation tests are not installed.

\iffalse
\begin{verbatim}
PROJECT_ROOT/
    +-- setup.py
    +-- src/
    |   +-- mypkg/
    |       +-- __init__.py
    |       +--  app.py
    |       +--  view.py
    +-- tests/
        +-- __init__.py
        +-- conftest.py
        +-- foo/
        |    +-- __init__.py
        |    +-- test_view.py
        +-- bar/
            +-- __init__.py
            +-- test_view.py
\end{verbatim}

Dadurch sind Quellcodes (\filename{src/}) und Tests (\filename{tests/})
sauber getrennt. Durch diese Trennung werden Tests nicht installiert.
\fi


\section{Example}
To keep the following example simple, the recommended test layout has
been omitted. We use two files in a directory \filename{pytest-x}:

\begin{itemize}
\item \filename{project.py}: contains the source code
\item \filename{test\_project.py}: contains all tests for
\filename{project.py}
\end{itemize}

\subsection{The Project to be Tested}
Our \filename{project.py} file contains:

\begin{python}
# Content of project.py
def add(x, y):
    """Expects two digits or strings; adds or concats
    both and return the result
    """
    return x + y
\end{python}

\subsection{The Test File \filename{test\_project.py}}
A test file for pytest will usually import the project to
be tested (here: \filename{project.py}).

Within a test function \pyth{assert} is used to compare the
real result with the expected result:

\begin{python}
# Content of test_project.py
import project

def test_add_numbers_1_2():
    assert project.add(1, 2) == 3

def test_add_strings_a_b():
    assert project.add("a", "b") == "ab"
\end{python}

\section{Running Tests}
The pytest-framework allows you to run specific tests or
all tests (by file, marker or test ID).

\subsection{Collecting all Found Test Functions}
\label{sec.pytest.collect-only}
Use the option \option{-{}-collect-only} to show you all tests
that were found:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest --collect-only
[...]
collected 2 items
<Module 'test_project.py'>
  <Function 'test_add_numbers_1_2'>
  <Function 'test_add_strings_a_b'>
\end{Verbatim}

The example shows that the module \filename{test\_project\_pytest.py} 
contains two test functions.


\subsection{Running all Tests}
\label{sec.pytest.test-all}
Without further arguments, pytest searches all tests and executes them:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest
[...]

test_project.py \textcolor{green}{..}                                   [100%]

\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}

If you prefer a more detailed view, use the \option{-v} option to display
the executed test functions (see also section~\ref{sec.config.addopts}):

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v
[...]

test_project.py::test_add_numbers_1_2 \textcolor{green}{PASSED}         [ 50%]
test_project.py::test_add_strings_a_b \textcolor{green}{PASSED}         [100%]

\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}


\subsection{Restricting to Test Files}
\label{sec.pytest.test-files}
If you want to execute only one or more test files, write them as arguments:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest test_project.py
[...]

test_project.py \textcolor{green}{..}                                   [100%]
\textcolor{green}{================= 2 passed in 0.02 seconds ================}
\end{Verbatim}


\subsection{Restricting to Test Functions (Test-ID)}
\label{sec.pytest.test-id}
If you are working on a specific test function, you may not want to run your
entire test suite.
You can restrict the execution to exactly this test function.
For this purpose, pytest knows the following syntax (so-called \emph{Test-ID}):

\begin{Verbatim}
<TEST_FILE>::<TEST_NAME>
\end{Verbatim}

Es dürfen eine oder mehrere Einträge angegeben werden:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_numbers_1_2
[...]

test_project.py::test_add_numbers_1_2 \textcolor{green}{PASSED}         [100%]
\textcolor{green}{================= 1 passed in 0.02 seconds ================}
\end{Verbatim}

If the test function is parameterized, the syntax is extended by the parameters:

\begin{Verbatim}
<TEST_DATEI>::<TEST_NAME>[Parameter1-Parameter2-...]
\end{Verbatim}

The test function from section~\ref{sec.pytest.mark.parametrize}
is called like follows:

\begin{Verbatim}[commandchars=\\\{\}]
[...]
test_project.py::test_eval[3+5-8] \textcolor{green}{PASSED}             [___%]
test_project.py::test_eval[2+4-6] \textcolor{green}{PASSED}             [___%]
\end{Verbatim}

If you want only the first one to be executed on the next pass,
write in the shell:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_eval[3+5-8]
\end{Verbatim}


\section{Understanding Failed Tests}
All tests in the previous sections were successful.
In this section we want to recreate a test that fails.

We want to add a new function \func{sub()} to our files,
which subtracts two numbers:

\begin{enumerate}
\item In \filename{test\_project.py} we write our test first:

\begin{python}
def test_sub_numbers_3_1():
    assert project.sub(3, 1) == 2
\end{python}

\item In \filename{project.py} we implement our (faulty) function
  \func{sub}:

\begin{python}
def sub(x, y):
    return x + y  # should be minus!
\end{python}
\end{enumerate}

On execution you get:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest
[...]

test_project.py \textcolor{green}{..}\textcolor{red}{F}                                  [100%]

========================= Failure =========================
\textcolor{red}{___________________ test_sub_numbers_3_1 __________________}
    def test_sub_numbers_3_1():
\textcolor{red}{>       assert project.sub(3, 1) == 2}
\textcolor{red}{E       assert 4 == 2}
\textcolor{red}{E        +  where 4 = <function sub at 0x...>(3, 1)}
\textcolor{red}{E        +    where <function sub at 0x...> = project.sub}

\textcolor{red}{test_project.py}:11: AssertionError
\textcolor{red}{============ 1 failed, 2 passed in 0.15 seconds ===========}
\end{Verbatim}

pytest shows you exactly \emph{where} something failed (in the test function
\func{test\_sub\_numbers\_3\_1}) and \emph{what} was compared
(\pyth{assert 4 == 2}).
So you can go troubleshooting and see that the operator in the function
\func{sub()} is wrong (should be minus instead of plus).


\section{Reviewing Expected Exceptions}
A good test suite tests the expected behavior both when the input
is correct and when the input throws an exception.

Our project should check the type in the function \func{add(x, y)}.
It should reject mismatched data types in the arguments with a \pyth{TypeError}
This requires a \emph{refactoring} of the project:

\begin{enumerate}
\item First the test is integrated in the file \filename{test\_project.py}:

\begin{python}
def test_instance_a_2():
    with pytest.raises(TypeError):
        add("a", 2)
\end{python}

\item Then the implementation is done in the file \filename{project.py}:

\begin{python}
def add(x, y):
    if isinstance(x, type(y)) or isinstance(y, type(x)):
        raise TypeError("Incompatible value")
    return x + y
\end{python}


\item Result on execution:
\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_both_a_2
[...]
test_project.py::test_instance_a_2 \textcolor{green}{PASSED}         [100%]
\end{Verbatim}
\end{enumerate}


\section{Marking of Test Functions}
There are two types of markers: \emph{built-in} and \emph{user-defined}.
For both you use the \func{pytest.mark} decorator.

With markers you can group test functions by certain properties
(compatible operating system, allowed Python version, execution time,
etc.) and select (or deselect) them on execution.

You can display all available markers with the command \cmd{pytest -{}-marker}.

\subsection{Using Built-In Markers}
Built-in markers are \func{skip}, \func{skipif} and \func{xfail}.
The \func{parametrize} marker is a special case and is described in
section~\ref{sec.pytest.mark.parametrize}.


\begin{description}
\item[Skipping of Test Functions]
To avoid having to comment out a test function, mark it with \func{skip}:

\begin{python}
@pytest.mark.skip(reason="needs more time")
def test_the_difficult_function():
\end{python}

\item[Conditional Skipping]
The \func{skipif} marker can be useful when skipping is conditional.
The following function is only executed if Python version 3.6 or higher
is used:

\begin{python}
import sys
@pytest.mark.skipif(sys.version_info < (3, 6),
                    reason="requires python3.6 or higher")
def test_function():
\end{python}

If a marker is needed more often, it is useful to define it once and
then use the variable:

\begin{python}
import pytest

py36_marker = pytest.mark.skipif(sys.version_info < (3, 6),
                  reason="requires python3.6 or higher")

@py36_marker
def test_function():
\end{python}

\item[Expected Error]
You use the \func{xfail} marker to mark test functions that fail:

\begin{python}
@pytest.mark.xfail
def test_function_to_fail():
\end{python}

This can be useful if the implementation is not yet ready,
but you know that the test function may not succeed.
In a way, it is like a TODO list.
\end{description}

\subsection{Leveraging User-Defined Markers}
\label{sec.marker.usedefined}

If the built-in markers are not sufficient, you can define your own
For example, you can group test functions by web aspects
(browser vs.\ CLI), runtime (fast vs.\ slow) etc.

The following test function is marked as \gquote{\func{slow}} to
indicate that this function takes longer to execute:

\begin{python}
import pytest

@pytest.mark.slow
def test_search_for_prime_numbers():
\end{python}

By marking it, you can choose when to execute:

\begin{itemize}
\item Select only tests marked with \func{slow}:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v -m slow
\end{Verbatim}

\item Select all other tests that are \emph{not} \func{slow}:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v -m "not slow"
\end{Verbatim}
\end{itemize}


\section{Parametrizing of Test Functions}
\label{sec.pytest.mark.parametrize}
Typically, test functions contain data that is \gquote{hardcoded}
in the function body. 
However, if a test function is to be able to accept different data,
different functions would have to be written for each combination.

To minimize this effort use the decorator \func{pytest.mark.parametrize}.
It expects:

\begin{itemize}
\item 1st argument contains a string.

Content is a comma separated list of arguments.
The number depends on the mandatory arguments of the function to be tested.
Additionally (typically) a \verb!expected! is specified.

\item 2nd argument contains a list of tuples.

They contain the data and the expected value.
\end{itemize}

For example, if the Python function \pyth{eval(source)}
is to be tested, it expects a mandatory argument,
namely the Python code.

Our test data is a tuple and each contains Python code and the result of it.
From this follows:

\begin{python}
# Inhalt von test_project.py
import pytest

@pytest.mark.parametrize("source, expected", [
  # First test:
  ("3+5", 8),
  # Second test:
  ("2+4", 6),
])
def test_eval(source, expected):
   assert eval(source) == expected
\end{python}

When pytest calls this test function, the first test will proceed as
follows:

\begin{enumerate}
\item The first tuple \pyth{("3+5", 8)} is taken from the test data.
\item The tuple is unpacked; \field{source} contains \verb!"3+5"! and
    \field{expected} the value \verb!8!
\item The obtained values are passed to the function \func{test\_eval}.
\item The variables are inserted into the function and executed;
    the \pyth{assert} statement becomes: \pyth{assert eval("3+5") == 8}.
\item The result is tested. If it's true, everything is okay. If not,
    an \pyth{AssertionError} exception is raised.
\item Pytest takes another tuple from the test data and repeats the procedure.
\end{enumerate}


\iffalse
Test-Funktionen benötigen Eingabewerte. Jedoch können Werte, z.~B.\ bei
Ganzzahlen Null, negative oder positive Werte annehmen. Diese alle zu
testen würde für jede Kombination eine eigene Test-Funktion benötigen.

Um diesen Aufwand zu minimieren, kannst du den Decorator
\func{pytest.mark.parametrize} verwenden:

\begin{python}
# Inhalt von test_project.py
import pytest

@pytest.mark.parametrize("source, expected", [
  # First test:
  ("3+5", 8),
  # Second test:
  ("2+4", 6),
])
def test_eval(source, expected):
   assert eval(source) == expected
\end{python}

\begin{itemize}
\item Das erste Argument enthält eine Zeichenkette. Sie enthält
komma-separierte Argumentnamen.
Üblicherweise enthält es die Anzahl der Argumente welche deine zu
testende Funktion erwartet. Zusätzlich wird ein Argument angegeben,
welches das erwartete Ergebnis enthält (typischerweise als
\verb!expected! benannt).

\item Das zweite Argument ist eine Liste mit Tupeln. Jedes dieser Tuple
enthält einen Testfall. Der Inhalt eines Tuples beschreibt die Daten welche die
zu testende Funktion übergeben bekommt und das erwartete Ergebnis.
\end{itemize}

Diese Tupel werden der Reihe nach in die Test-Funktion eingesetzt. Dadurch
ist es sehr einfach weitere Testdaten hinzuzufügen ohne die eigentliche
Test-Funktion zu ändern.

Beim Ausführen erscheint:

\begin{Verbatim}[commandchars=\\\{\}]
$ pytest -v test_project.py::test_eval
...
collected 2 items

test_project.py::test_eval[3+5-8] \textcolor{green}{PASSED}              [ 50%]
test_project.py::test_eval[2+4-6] \textcolor{green}{PASSED}              [100%]
\end{Verbatim}
\fi

\section{Using Fixtures}
\gquote{Test fixtures initialize test functions.}
Fixtures are functions marked with the decorator \func{pytest.fixture}.
% Thus pytest can recognize that a fixture is involved.

\subsection{Listing Available Fixtures}
To get an overview of all fixtures inside your project use the
command \cmd{pytest -{}-fixtures}.

\subsection{Using Built-In Fixtures}
\label{sec.fixture.builtin}
Built-in fixtures are e.g. \func{cache}, \func{capsys}, \func{pytestconfig},
\func{tmpdir} and others.
In this section, we will go into more detail about the \func{tmpdir} fixture.
Details can be found in \cite{krekel.pytest.api.fixtures}.

The \func{tmpdir} fixture creates a temporary directory that is unique to
each test function and is named differently for each run.
As return value you get a \module{py.path.local}\cite{py.path} object.

In the following example, the test function uses the \func{tmpdir} fixture
and creates a file in the temporary directory:

\begin{python}
def test_create_file(tmpdir: py.path.local):
    # Create path to hello.txt:
    hello = tmpdir.join("hello.txt")
    # write text to hello.txt:
    hello.write_text("Hello\nWorld", encoding="utf-8")
    # Now call your test function with the string of the path:
    assert count_linebreaks(hello.strpath) == 1
\end{python}

With the fixture, you don't have to create the temporary directory
manually.

\subsection{Using Custom Fixtures}
You can write custom fixtures in a test file or in the file
\filename{conftest.py}.
The latter has the advantage that pytest loads this file automatically,
so there is no need to import it.

More exciting possibilities are shown in \cite{krekel.pytest.fixtures}.
All examples need a \pyth{import pytest} which is not explicitly specified in the code.

\iffalse
\subsubsection{Returning Data}
Im folgenden Beispiel wird eine Fixture geschrieben, welche eine Liste (=Daten)
zurückgibt. Es sind die ersten fünf Werte der Fibonacci-Folge:

\begin{python}
@pytest.fixture
def fibonacci_5():
    return [0, 1, 1, 2, 3, 5]
\end{python}
\fi


\subsubsection{Using Built-in Fixtures as Dependency}
Fixtures can also use other fixtures. For example, you can
create a file in a temporary directory and feed it with certain data.


\begin{python}
@pytest.fixture
def datafile(tmpdir):
   hello = tmpdir.join("hello.txt")
   hello.write_text("Hello\nWorld", encoding="utf-8")
   return hello
\end{python}

With this fixture the code of the test function \func{test\_create\_file}
from section~\ref{sec.fixture.builtin} can be simplified as follows

\begin{python}
def test_create_file(datafile: py.path.local):
    assert count_linebreaks(datafile.strpath) == 1
\end{python}


\subsubsection{Parametrizing of Fixtures}
If you want your fixture to use different values,
you can parameterize a fixture.
The following fixture reads a configuration file from the user
directory or from \filename{/etc} and the test function is called twice:

\begin{python}
import pytest

@pytest.fixture(params=["~/.config/foo", "/etc/fooconfig"])
def config(request):
    configfile = open(request.param, 'r')
    yield configfile
    config.close()

def test_fooconfig(config):
    # test if you can read a line:
    assert config.readline()
\end{python}

\subsection{Defining Scopes for Fixtures}
The \func{pytest.fixture} decorator can also take an argument
\field{scope}. Possible values of \field{scope} are:
\field{function} (default), \field{class}, \field{module},
\field{package} or \field{session}.

With a \emph{scope}, pytest can call a fixture for each function
again or limit it on classes, modules, packages or the whole session.

Suppose you write tests to test SSH service. For this you have written a SSH fixture.
However, creating and destroying the SSH object is time consuming.
To not to waste time with every test that requires the SSH service, the
fixture is limited to the session.
Thus, each test function gets the same SSH object:

\begin{python}
# in conftest.py
import pytest

@pytest.mark.fixture(scope="session")
def ssh_service():
    #  creates an SSH service object
    yield service_object
    # destorys the SSH service object

# in a test file:
def test_git_amount(ssh_service):
    # test the SSH service
\end{python}

When pytest is started, the SSH service is also started.
After the last test function is executed, the SSH service is stopped.

% --------------------------------------------------------------------
\begin{thebibliography}{10}
\bibitem{beck.tdd}
\bauthor{Kent Beck},
\btitle{Test-Driven Development by Example},
Addison Wesley, ISBN 978-0-321-14653-3.

\bibitem{py.path}
\bauthor{Holger Krekel} et.~al.
\btitle{py.path},
\url{https://py.readthedocs.io/en/latest/path.html}

\bibitem{krekel.pytest.cmd.option}
\bauthor{Holger Krekel} et.~al.,
\btitle{Configuration Options},
\url{https://docs.pytest.org/en/latest/reference.html#configuration-options}

\bibitem{krekel.pytest.api.fixtures}
\bauthor{Holger Krekel} et.~al.,
\btitle{API: Fixtures},
\url{https://pytest.org/en/latest/reference.html#fixtures}

\bibitem{krekel.pytest}
\bauthor{Holger Krekel} et.~al.,
\btitle{pytest: helps you write better programs},
\url{https://pytest.org}

\bibitem{krekel.pytest.fixtures}
\bauthor{Holger Krekel} et.~al.,
\btitle{pytest fixtures: explicit, modular, scalable},
\url{https://pytest.org/en/latest/fixture.html}

\bibitem{maries.pypack}
\bauthor{Ionel Cristian Mărieș},
\btitle{Packaging a Python Library},
\url{https://blog.ionelmc.ro/2014/05/25/python-packaging/}

\iffalse
\bibitem{pytest-cov}
\bauthor{Marc Schlaich} et.~al.,
\btitle{Coverage Reports for pytest},
\url{https://github.com/pytest-dev/pytest-cov}
\fi

\bibitem{usersnap.com}
\bauthor{John Sonmez}
\btitle{7 Common Types of Software Testing}
\url{https://usersnap.com/blog/software-testing-basics/}

\bibitem{wikipedia.tdd}
\bauthor{Wikipedia},
\btitle{Test-driven development}
\url{https://en.wikipedia.org/wiki/Test-driven_development}

\end{thebibliography}

\end{multicols}
\end{document}
