\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

\usepackage[simplified]{pgf-umlcd}

%opening
\title{Python3 OOP Cheatsheet}
% \titlehead{fooo}
\subtitle{Objectoriented Programming Part 1: Basics}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}


%% -----------------------------------------------------------
\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\begin{quote}
Think about if you really need classes. Perhaps
modules, functions, generators, or context managers make
more sense in your case?
See \emph{Stop Writing Classes} in \url{https://www.youtube.com/watch?v=o9pEzgHorH0}.
\end{quote}


\section{Terminology}
\label{sec.terms}
Used terminology in this cheatsheet (from \cite{python.org.glossary}):

\begin{description}
\item[Attribute]
A value associated with an object which is referenced by name using dotted expressions.

\item[Datatype]
A unit of a value range and its operations.
Typical datatypes are integers, floats, strings, lists etc.

\item[Instance]
Actual object of a specific class.

\item[Instantiation]
Procedure for creating a new instance. See section~\ref{sec.instanciate}.

\item[Instance variable]
Part of the state of an instance.
A variable that contains each instance, but whose value is different.
See section~\ref{sec.instvar}.

\item[Class]
A \gquote{template} or blueprint that defines the variables and the
methods (functions) that are common to all objects of a certain type.


\item[Class method]
Method which is marked with the \pyth{classmethod} decorator.
See section~\ref{sec.classmethod}.

\item[Class variable]
Part of the state of a class. A variable that only defines within a class
and does not belong to an instance.
See section~\ref{sec.classvar}.

\item[\gquote{Magic Method}]
Methode, die mit einem doppelten Unterstrich beginnt und endet.
Siehe Abschnitt~\ref{sec.dunder}.

\item[Method]
A defined function within the class body. Implements the behavior of a class.
See section~\ref{sec.methods}.

\item[Message]
Data that is exchanged between two objects.

\item[Namespace]
Scope for names. See section~\ref{sec.namespaces}.

\item[Object] % TODO: translate
Specimen of a class.

\iffalse
\item[Static Method]
Method which is marked with the \pyth{staticmethod} decorator.
See section~\ref{sec.staticmethod}.
\fi
\end{description}

\iffalse
\section{Programming Paradigms}
\label{sec.paradigm}
Ein Paradigma ist ein Denk- oder Handlungsmuster zur Formulierung
einer Problemlösung. Typische Programmierparadigmen sind:

\begin{description}
\item \emph{Imperativ-Prozedurales Programmierparadiga}
Eine Folge von Befehlen in einer bestimmten Reihenfolge.
Ein prozedurales Programm teilt die Aufgaben in Teilaufgaben bzw.
Prozeduren auf.

\item \emph{Deklarativ-Funktionales Programmierparadiga}
Der Schwerpunkt basiert auf dem \emph{wie} und nicht dem \emph{was}.
Der Programmierer deklariert Eigenschaften des gewünschten
Ergebnisses, jedoch nicht wie es berechnet wird.
Ein funktionales Programm beschreibt das gewünschte Ergebnis
als Wert einer Reihe von Funktionsanwendungen.

\item \emph{Objektorientiertes Programmierparadiga}
Menge von interagierenden Objekten. Objekte und Klassen sind
eine Einheit von Daten und Methoden. Durch \emph{information hiding} und
\emph{encapsulation} kann der Zugriff von außen beschränkt werden.
\end{description}

Weitere Programmierparadigmen findest du in \cite{wikipedia.paradigma}.
\fi

\iffalse

% Abgesehen von der vorigen Warnung, die üblichen Vorteile, die bei OOP
% genannt werden, sind:

\begin{description}
\item[Organisation]
Durch OOP (bzw. Klassen) kannst du sowohl Daten als auch Funktionen
zusammenfassen die eine Einheit bilden. Du bist nicht darauf angewiesen,
komplexe oder verschachtelte Datenstrukturen (dicts in einem dicts)
zu verwenden, sondern kannst die Teile korrekt benennen und sie
bequem referenzieren.

\item[Zuständigkeit]
Eine Klasse hat genau eine Verantwortung, nicht zwei oder drei.
Dadurch erfordert OOP nachzudenken, welche Verantwortlichkeiten
eine Klasse hat und wie sie mit anderen Klassen in Beziehung steht.
%OOP hilft den Zustand eines Teils im Auge zu behalten, d.~h.\ du
%kannst alle Informationen in einer Klasse sammeln.

\item[Schnittstellen (APIs)]
Mittels OOP können genau definierte Schnittstellen einer
Klasse entwickelt werden ohne dass man die Implementierung
verstehen muss.

%\item[Wiederverwertung]
%OOP-Code der einmal geschrieben und gut getestet wurde, lässt sich
%überall wiederverwenden.

\item[Datenkapselung]
Je nachdem wie du deine öffentliche API einer Klasse definierst,
kannst du Funktionen und Daten vom Anwender verstecken.
Dadurch kannst du die Implementierung anpassen, ohne den Code zu
ändern.

\item[Vererbung]
Vererbung erlaubt, Daten und Funktionen an einer Stelle zu
definieren und wenn gewünscht, sie an anderer Stelle zu erweitern.
\fi

\section{The four Pillars of OOP}
\label{sec.four-pillars-of-oop}

There are four main principles that make a language object-oriented:

\begin{description}
\item[Encapsulation]
Process of combining data elements and methods into one unit and
protect them if necessary.
Data encapsulation is achieved by a class.

Class data is usually private and protected from direct access
from outside (\gquote{black box}). Access is only possible via
defined interfaces (\gquote{API}). This is called
\emph{information hiding}.

\item[Abstraction]
Process of showing only the essential/necessary features of an
object to the outside and hiding the remaining, irrelevant information.


\item[Inheritance]
Relationship between two or more classes (\gquote{is-a}/\gquote{has-a} relationship).
An inheritance creates a specialization for the purpose of reusing code.
See OOP cheatsheet part 2.


\item[Polymorphismus]
From Greek. Refers to the ability to process objects differently depending
on their data type or class. In other words, it allows you to define an
interface, but can contain different implementations.
\end{description}


\section{Properties of Python Classes}
\label{sec.classes}
A Python class is the sum of all object attributes with the properties:

\begin{itemize}
\item All classes are implicitly derived from the base class \pyth{object}.

\item Is introduced with the keyword \pyth{class}.

\item Python distinguishes between creating a class (via \dunder{new})
    and initialization (\dunder{init}). Usually only the latter is needed.
    See section~\ref{sec.construct}.

\item Only in rare cases a destructor method (\dunder{del}) is needed,
    because the garbage collector does this automatically (but not
    if you think it will do it).

\iffalse
\item Conventions:
\begin{itemize}
\item Erstes Argument einer jeden Methode enthält das Instanzobjekt, per
Konvention als \pyth{self}. Siehe Abschnitt~\ref{sec.instancemethods}.
\item In manchen Fällen erwarten Methoden die Klasse (per Konvention
als \pyth{cls}). Siehe Abschnitt~\ref{sec.classmethod}.
\item Methoden oder Variablen die mit einem Unterstrich (\verb!_!)
anfangen, werden als privat angesehen. Siehe Abschnitt~\ref{sec.underscore}.
\item Methoden die mit zwei Unterstrichen anfangen und aufhören (\verb!__NAME__!)
sind sog. \gquote{magic methods}. Siehe Abschnitt~\ref{sec.dunder}.
\end{itemize}
\fi
\end{itemize}

\iffalse
\section{Recommendations for Classes}
\label{sec.recommendations}
Eine Warnung vorab:

\begin{quote}
Überlege, ob du wirklich eine Klasse brauchst. Vielleicht
sind Module, Funktionen, Generatoren oder Context Manager besser?
Siehe \emph{Stop Writing Classes} in \url{https://www.youtube.com/watch?v=o9pEzgHorH0}.
\end{quote}

\subsection{Conceptual Recommendations}
\begin{itemize}
\item Schreibe eine Klasse so, dass sie nur \emph{einen} Zweck bzw.\ Aufgabe
erfüllt.

\item Schreibe eine Methode so, dass sie eine Aktion abbildet.

\item Verberge deine Implementierung vor der Außenwelt
(engl.\ \emph{information hiding}).

\item Verwende eine Klasse, wenn du die Standardoperationen wie
Addition, Subtraktion usw.\ überschreiben musst.

\item Wirf Ausnahmen (engl. \emph{exceptions}), um unerlaubte
Werte zurückzuweisen.

\item Vermeide Annahmen zur Implementierung bei anderen Klassen.

\item Vermeide zu enge Verbindungen bei dem verschiedene Objekte sich
wie ein Objekt verhalten.

\end{itemize}

\subsection{Recommendations for Implementations}
\begin{itemize}
\item Schreibe den ersten Buchstaben des Klassennamens groß (PEP8\cite{pypa.pep8}).

\item Initalisiere möglichst alle Variablen die zur Instanz gehören
innerhalb von \dunder{init}.

\item Initialisiere keine neuen Variablen außerhalb von \dunder{init}.

\item Implementiere die Methoden \dunder{init} und
\dunder{repr}/\dunder{str}\cite{dbader.repr}.

\item Vermeide Getter- und Setter-Methoden sondern nutze Properties.
Siehe Abschnitt~\ref{sec.properties}.
\end{itemize}
\fi

\section{Example of a Class: Point}
\label{sec.class.example}
To map a point, the following properties should apply:

\begin{itemize}
\item Maps X/Y coordinates in a Cartesian coordinate system.
\item Expects two coordinates (X and Y) as \pyth{float} type.
\item Set to \pyth{0.0} if nothing is specified.
\item Contains a method \pyth{distance} which calculates the
    distance to another point.
\item Contains the methods \dunder{str} and \dunder{repr} to
    output the instance.
\end{itemize}

There are different ways to represent a point:

\begin{itemize}
    \item Store X and Y coordinates separately as two independent variables.
    \item As list or tuple.
    \item As new data type (\pyth{Point} class) for points.
\end{itemize}

\subsection{Code}
\begin{python}
import math
class Point:
   """A point in 2D space"""
   def __init__(self, x=0, y=0):
      self.x, self.y = x, y
   def __repr__(self):
      return "{}{}".format(type(self).__name__,
                           str(self))
   def __str__(self):
      return "({0.x!r}, {0.y!r})".format(self)
   def __iter__(self):
        yield from (self.x, self.y)
   def distance(self, other=None):
      if other is None:
         other = type(self)()  # = Point(0, 0)
      dxy = [x - o for x, o in zip(other, self)]
      return math.hypot(*dxy)
\end{python}
\pagebreak[3]

\subsection{UML Notation}

\begin{center}
\begin{tikzpicture}
\begin{class}{ Point }{0,0}
\attribute{+ x : float }
\attribute{+ y : float}
\operation{+ \dunder{init}(self, x=0.0, y=0.0) : }
\operation{+ \dunder{repr}(self) : str }
\operation{+ \dunder{str}(self) : str }
\operation{+ \dunder{iter}(self) :  generator}
\operation{+ \func{distance}(self, other) : float}
\end{class}
\end{tikzpicture}
\end{center}

\iffalse
\begin{tikzpicture}
\umlclass{Point}%
{% Attribute
 \textbf{Attribute}\\
 \# x: int \\
 \# y: int
}%
\end{tikzpicture}
\fi


\subsection{Instanciation of the Point Class}
\label{sec.instanciate}
\emph{Instantiation} creates a new object (an \gquote{instance})
of the class \pyth{Point}.
The class is called by round brackets (similar
like a function call) and creates the instance as result:

\begin{python}
>>> p1 = Point()
>>> p2 = Point(1,1)
>>> p1, p2
(Point(0, 0), Point(1, 1))
\end{python}

You can create as many instances as you like from a class
(only limited by memory). Each instance is unique and typically
contains different values.

\subsection{Call Methods}
Methods are called using the dot notation
\pyth{inst.method(args)}:

\begin{python}
>>> p1.distance(p2)
1.4142135623730951
\end{python}


\section{What is \pyth{self}?}
\label{sec.self}
The variable name \pyth{self} is a convention similar to
\verb!this! in C++: it is a reference to the instance.

While you could change the name \pyth{self} as you wish,
you should not do this to avoid confusion.
% The meaning always remains the same.

If a class \pyth{Cls} contains a method \pyth{meth}, the
instance object \pyth{inst} can be called in two ways:

\begin{verbatim}
inst.meth(*args) == Cls.meth(inst, *args)
\end{verbatim}

Both calls produce the same result.
So the call \pyth{p1.distance(p2)} can be written like this:

\begin{python}
>>> Point.distance(p1, p2)
1.4142135623730951
\end{python}

The argument p1 is \pyth{self}. Python converts the notation
\pyth{p1.distance(p2)} internally.


\section{Creating and Initializing an Instance}
\label{sec.construct}
Python does not know a \emph{constructor} in the sense of C++ or Java.
In Python, a \gquote{constructor} would split into two parts:

\begin{description}
\item[Instance Creator: \pymagic{new}(cls, {[args]*})]\hfill\\

The first argument is a reference to the class.
Typically used to adapt immutable data types before it
is initialized with \dunder{init}.

\item[Initializer: \pymagic{init}(self, {[args]*})]\hfill\\

The first argument, \pyth{self}, is always the instance.
Typically used to initialize the instance variables, but it
returns nothing(!). Often erroneously called \gquote{constructor}.

\end{description}

Normally \pymagic{new} is rarely used, most often you will use
\pymagic{init}.

When Python creates a new instance, the following two steps are
performed internally:

\begin{enumerate}
\item First create a new instance using \pymagic{new}:

\begin{python}
>>> p = object.__new__(Point)
\end{python}

Since \verb!p! has not yet been initialized, it cannot be displayed:

\begin{python}
>>> p
<repr(<__main__.Point at 0x7fc378044f90>)
   failed: AttributeError:
   'Point' object has no attribute 'x'>
\end{python}

But the type is \pyth{Point}:

\begin{python}
>>> type(p)
__main__.Point
\end{python}

\item Initializes the created instance with \pymagic{init}:

\begin{python}
>>> Point.__init__(p, x=1, y=2)
\end{python}

Here \pyth{p} acts as \pyth{self}.
Now the output works too:

\begin{python}
>>> p
Point(x=1, y=2)
\end{python}

\end{enumerate}

Usually you will not have to do this manually, but Python will
do it for you in the background.
A typical instantiation of \pyth{Point} shows
section~\ref{sec.instanciate}.


\section{Analysing an Object with \pyth{dir}}
To analyze objects more precisely, use the function \pyth{dir(obj)}.
It displays all attributes of an object:

\begin{python}
>>> dir(p1)
[ # omitted all names with "__" before and end
 'distance', 'x', 'y']
\end{python}


\section{The \pyth{__class__} Attribute}
Each instance has a \pyth{__class__} variable of type \pyth{type}.
This contains some important variables:

\begin{description}
\item[\selfdunder{class}{}] the class to which an instance belongs.
    Equivalent to \pyth{type(self)}.

\item [\selfdunder{class}{name}] name of the class.
    Equivalent to \pyth{type(self).__name__}.
    
\item [\selfdunder{class}{base}] the base class if a class was
    derived or \pyth{object}.

\item[\selfdunder{class}{bases}] a tuple of base classes (in
    case of multiple inheritance).
    
\item[\selfdunder{class}{module}] the name of the module in which
    the class was defined.
\end{description}


\section{Dynamic Checking, Returning, Deleting and Setting of Attributes}
\label{sec.attrs}
To dynamically set, read, check, or delete an attribute from an object.
The variable \pyth{name} is of type \pyth{str}:

\begin{description}
\item [\func{delattr}(obj, name)]
    Deletes the \field{name} attribute from \pyth{obj}.
    Identical to \pyth{del obj.name}

\item[\func{getattr}(obj, name{[, default]})]
    Returns the value of the \field{name} attribute of \pyth{obj}.
    Identical with \pyth{obj.name}. If the specified attribute does not
    exists, \field{default} is returned.
    Otherwise a \pyth{AttributeError} is raised.
    
\item[\func{hasattr}(obj, name)]
    Returns \pyth{True} if \pyth{obj} has an attribute with
    the name \field{name}. Otherwise \pyth{False}.
    
\item [\func{setattr}(obj, name, value)]
    Assigns the attribute \field{name} (a string) of the object \pyth{obj}
    with the specified \pyth{value}.
    Identical with \pyth{obj.name = value}.
\end{description}

\begin{python}
>>> p = Point(1, 4)
>>> hasattr(p, 'x')
True
>>> getattr(p, 'x')  # = p.x
1
\end{python}

\section{Namenspaces}
\label{sec.namespaces}
\emph{Namespaces} are scopes in which a name (object) is valid:

\begin{description}
\item [Local namespace] contains names within a function or method
    (via \pyth{locals()}).
\item [Modular Namespace] defines names within a file (via \pyth{globals()}).
\item [Built-in namespace (\emph{builtins})] contains names that are
    always valid in Python (via \pyth{__builtins__} or module \module{builtins}).
\item [Class Namespace] contains all methods and leave variables
    (via \pyth{vars(MyClass)} or \pyth{MyClass.__dict__}).
\item [Instance Namespace] contains all instance variables
    (via \pyth{vars(inst)} or \pyth{inst.__dict__}).
\end{description}

\begin{python}
class Foo:
   clsvar = 42
   def __init__(self, x):
      self.x = x
$\comment{Class namenspace contains class variable:} $
>>> vars(Foo)
mappingproxy({..., 'clsvar': 42})
$\comment{Instance namenspace contains instance variable:}$
>>> f = Foo(10)
>>> vars(f)
{'x': 10}
\end{python}

\iffalse
Let's look at the following code:

\begin{python}
class MyClass:
    var = 1  # Keine Punktschreibweise notwendig
    def __init__(self, value):
        self.ivar = value

inst = MyClass(42)
\end{python}

Each Python class and each instance of it has its own different namespace. This is stored in \pyth{__dict__},
i.e. there is \pyth{MyClass.__dict__} and \pyth{inst.__dict__}.
\fi

If Python needs an attribute or method, it proceeds as follows:

\begin{enumerate}
\item Query the instance namespace \pyth{inst.__dict__} first.
    If the object is found, the requested value is returned.
\item If the object was not found in the instance namespace,
    the class namespace \pyth{MyClass.__dict__} is looked up.
    If the object is found there, the requested value is returned.
\item If the object still could not be found, raise \pyth{AttributeError}.
\end{enumerate}

\section{Unterscore Conventions in Names}
\label{sec.underscore}
The underscore in variable, method, class, and object names
has a special meaning:

\begin{description}
\item[\texttt{\_simple\_leading\_underscore}]
    Convention(!) for declaring \emph{pseudo-private} attributes or
    classes within modules.
    Note that this is \emph{not really private} like C++ or Java.
    It is just a \gquote{weak, internal hint}.

\item[\texttt{\_\_double\_leading\_underscoe}]
    Attribute name is changed through the so-called \emph{name mangeling}.
    The attribute name is preceded by an underscore and the class name. 
    \pyth{__foo} in the class \pyth{Bar} becomes the changed attribute
    name \pyth{_Bar__foo}.
    It is used to avoid name conflicts between classes.

\item[\texttt{\_\_double\_leading\_and\_concluding\_underscoe\_\_}]
So called \emph{magic methods}. See section~\ref{sec.dunder}.
Do not use self-defined names with this spelling to avoid name conflicts.
\end{description}

\begin{python}
class Foo:
    public = 1
    _private = 2
    __mangled = 3

Foo.public # -> 1
Foo._protected # -> 2
Foo.__mangled # -> AttributeError
Foo._Foo__mangled # -> 3
\end{python}


\section{Class and Instance Variables}
\label{sec.attributs}
Variables within the class body can be readable or writeable.
There are two types of variables: Instance and class variables.

\subsection{Instance Variables}
\label{sec.instvar}
Instance variables are attributes belonging to an instance:

\begin{itemize}
\item Each instance has its own individual value.
\item Reading a value: \pyth{Inst.attr}.
\item Setting a value: \pyth{Inst.attr = VALUE}.
\item Deleting an instance variable: \pyth{del Inst.attr}.
\end{itemize}

In the previous example \pyth{x} and \pyth{y} are attributes,
i.~e.\ they \gquote{belong} to a certain instance and their values
are typically different:

\begin{python}
>>> p1.x, p1.y
(0, 1)
\end{python}

\subsection{Class Variables}
\label{sec.classvar}
Class variables are attributes that belong only to the class:

\begin{itemize}
\item Have the same value for all instances.
\item Reading a value: \pyth{MyClass.attr} or \pyth{Inst.attr}
      (see also section~\ref{sec.namespaces}).
\item Setting a value: \pyth{MyClass.attr = VALUE} \textbf{not(!)}
    with \pyth{Inst.attr = VALUE}.
\item Deleting a class variable: \pyth{del MyClass.attr}.
\end{itemize}

Class variables are used in the following use cases:

\begin{itemize}
\item Define constants:
\begin{python}
class Circle:
    pi = 3.14159
    # ...
\end{python}

\item Define standard values:
\begin{python}
class Robot:
    Three_Laws = (
        "A robot may not injure ....",
        "A robot must obey ...",
        "A robot must protect ..."
        )
    # ...
\end{python}

\item Register values for each instance call:
\begin{python}
class Person:
    register = []
    def __init__(self, name):
        self.name = name
        Person.register.append(name)
        # Alternative:
        # cls = type(self)
        # cls.register.append(name)

joe = Person('Joe')
bob = Person('Bob')
print(Person.register)
## ['Joe', 'Bob']
\end{python}

\item For performance reasons. Class variables are initialized
    during class definition while instance variables are
    assigned to instance variables each time a new instance is created.
\end{itemize}


\section{Methods}
\label{sec.methods}
Methods are functions within the class body:

\begin{itemize}
\item Instance methods. Know something about the instance
    or class and can manipulate both.
\item Class methods. Know only something about the class, but not the instance.
\item Static methods. Know neither about the instance nor about the class.
\end{itemize}

\subsection{Instance Methods und \pyth{self}}
\label{sec.instancemethods}
An instance method has the following properties:

\begin{itemize}
\item First argument is \emph{always}(!) a reference to
    the instance, by convention \pyth{self}.

\item Can take any number of arguments after the first argument \pyth{self}.

\item Can change the instance or the class.

\item Call via \pyth{Inst.method(arguments)}.
\end{itemize}

In the example with \pyth{Point} these are the methods
\pyth{distance()}, but also \dunder{repr} and \dunder{str}:

\begin{python}
>>> p2 = Point(1, 1)
>>> p1.distance(p2)
1.4142135623730951
\end{python}

\iffalse
The Dunder methods \dunder{repr} and \dunder{str} are indirectly controlled by
\pyth{repr()} and \pyth{str()} and take care of the correct passing from \pyth{self}:

\begin{python}
>>> repr(p1)
'Point(0, 0)'
>>> str(p2)
'(0, 0)'
\end{python}

Unusual, but possible, dunder methods can also be called directly.
Here, the \pyth{self} argument must be passed the respective instance,
which leads to the same result:

\begin{python}
>>> Point.__repr__(p1)
'Point(0, 0)'
>>> Point.__str__(p2)
'(0, 0)'
>>> Point.distance(p1, p2)
1.4142135623730951
\end{python}

Both function calls are allowed. However, the first with \pyth{repr()}
and \pyth{str()} is preferable to the direct call, because it is clearer.
\fi

\subsection{Class Methods und \pyth{cls}}
\label{sec.classmethod}
A class method has the following properties:

\begin{itemize}
\item First argument is always a reference to the class,
    by convention \pyth{cls}.
\item Can take any number of arguments after the first argument \pyth{cls}.
\item Can only change the class itself, not the instance.
\item Call via \pyth{MyClass.method(args)} or \pyth{Inst.method(args)}.
\item Requires the \pyth{classmethod} decorator:

\begin{python}
class Robot:
    __counter = 0
    def __init__(self):
        type(self).__counter += 1
    @classmethod
    def amount(cls):
        return cls.__counter

Robot()
Robot()
Robot.amount()  ## Result: 2
\end{python}
\end{itemize}

Class methods are used for the following reasons:

\begin{itemize}
\item As a \emph{factory method}. These are methods to cover
different use cases, similar to a constructor.
See \func{Fraction.from\_float()} and \func{Fraction.from\_decimal()} from the
\cmd{fractions} module.

\item Creating correct instances for inheritance:

\begin{python}
from datetime import date
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, name, birthyear):
        return cls(name, date.today().year - birthyear)
    def __repr__(self):
        return "{}: {} years old".format(self.name,
                                         self.age)

class Man(Person):
    pass

john = Man.fromBirthYear('John', 1985)
isinstance(john, Man) # -> True
\end{python}
\end{itemize}


\subsection{Static Methods}
\label{sec.staticmethod}
An static method has the following properties:

\begin{itemize}
\item The first argument is \emph{not}(!) a reference to a class or instance.
    The first argument is just a normal argument.

\item Can take any number of arguments.

\item Can neither change the class nor the instance,
    except via the publicly available interface.

\item Requires the \pyth{staticmethod} decorator:

\begin{python}
class Greet:
   @staticmethod
   def hello(name):
       return "Hello {}".format(name)

Greet.hello("Tux")
# -> "Hello Tux"
\end{python}
\end{itemize}


In many cases a static method has only limited use. It can usually
be defined as a function outside the class without loss.
Static methods are used mostly for reasons of structural membership
in a class.

For more information, see \cite{danjou.python}.

\section{\gquote{Magic Methods}}
\label{sec.dunder}
\emph{Magic methods} or \gquote{dunder methods} (double underscore)
are:

\begin{itemize}
\item Method name enclosed by two underscores like \pymagic{repr}.
\item Core of the OOP idea in Python.
\item Used to allow operators, accesses, assignments, etc.
\item Through magic methods, self-defined classes behave like built-in data types.
\end{itemize}

\iffalse
Sie werden verwendet, um Operatoren, Zugriffe, Zuweisungen u.~a.\ zu ermöglichen
und sind der Kern von OOP in Python. Durch magic methods verhalten sich
selbstdefinierte Klassen wie eingebaute Datentypen.
\fi

There are countless dunder methods to list them all here.
For more detailed information, see \cite{kettler.magicmethods,python.specialmethods}.
However, the most common magic methods are described in the following
subsections.

%\iffalse
\subsection{Construction and Initializing}

\begin{description}
\item[\pymagic{new}(cls {[,arg]}*)]
    creates and returns a new instance of a class.
    % May be defined via \pyth{staticmethod}.
    This method \emph{must} call \pymagic{new}() of the base class(es).
    See additionally \url{https://www.python.org/download/releases/2.2.3/descrintro/#__new__}.
\item[\pymagic{init}(self, {[,arg]}*)]
    initialize the instance (\pyth{self}) of the class.
\item[\pymagic{del}(self)]
     \emph{destructor method}, if required automatically called by the garbage
     collector. Rarely used.
\end{description}

\subsection{Strings and Formatierung}
\begin{python}
__repr__(self)         = repr(self)
__str__(self)          = str(self)
__format__(self, spec) = format(self, spec) $\comment{See \cite{python.formatspec}}$
\end{python}

% \begin{description}
% \item[\pymagic{repr}(self)] entspricht \pyth{repr(self)}
% \item[\pymagic{str}(self)] entspricht \pyth{str(self)}
% \item[\pymagic{format}(self, spec)] entspricht \pyth{format(self, spec)}. Siehe
% \cite{python.formatspec}.
% \end{description}

\subsection{Compare Methods}
\begin{python}
__eq__(self, other)    = self == other
__ne__(self, other)    = self != other
__lt__(self, other)    = self < other
__gt__(self, other)    = self > other
__le__(self, other)    = self <= other
__ge__(self, other)    = self >= other
\end{python}

% \begin{description}
% \item[\pymagic{cmp}(self, other)] grundlegende Vergleichsmethode.
% \item[\pymagic{eq}(self, other)] Vergleichsoperator \pyth{self == other}
% \item[\pymagic{ne}(self, other)] Vergleichsoperator \pyth{self != other}
% \item[\pymagic{lt}(self, other)] kleiner als \pyth{self < other}
% \item[\pymagic{gt}(self, other)] größer als \pyth{self > other}
% \item[\pymagic{le}(self, other)] kleiner oder gleich als \pyth{self <= other}
% \item[\pymagic{ge}(self, other)] größer oder gleich als \pyth{self >= other}
% \end{description}

\subsection{Methods for Ses (Sequences, Mappings)}
\begin{python}
__len__(self)            = len(self)
__contains__(self, item) = item in self
__iter__(self)           = iter(self)
__next__(self)           = next(self)
__getitem__(self, key)   = self[key]
__setitem__(self, key, value) = self[key] = value
__delitem__(self, key)   = del self[key]
__reversed__(self)       = reversed(self)
\end{python}

% \begin{description}
% \item[\pymagic{len}(self)] entspricht \pyth{len(self)}
% \item[\pymagic{contains}(self, element)] entspricht \pyth{element in self}
% \item[\pymagic{getitem}(self, key)] entspricht \pyth{self[key]} %\verb![key]!
% \item[\pymagic{iter}(self)] \pyth{iter(self)}, Teil des Iterationsprotokolls
% \item[\pymagic{next}(self)] \pyth{next(self)}, Teil des Iterationsprotokolls
% \item[\pymagic{getitem}(self, key)] \pyth{self[key]}, \pyth{self[i:j:k]},
% \pyth{x in self}
% \item[\pymagic{setitem}(self, key, value)] \pyth{self[key] = value}
% \item[\pymagic{delitem}(self, key)] \pyth{del self[key]}
% \item[\pymagic{reversed}(self)] \pyth{reversed(self)}
% \end{description}

\subsection{Methods for Numbers (Binary Operators)}

Methods that do not support an operation return the object
\pyth{NotImplemented} (no exception!).

\subsubsection{Basic Binary Methods}
\begin{python}
__add__(self, other)     = self + other
__sub__(self, other)     = self - other
__mul__(self, other)     = self * other
__truediv__(self, other) = self / other
__floordiv__(self, other) = self // other
__mod__(self, other)     = self % other
__pow__(self, other [, mod]) = pow(self, other [, mod])
                         = self ** other
__lshift__(self, other)  = self << other
__rshift__(self, other)  = self >> other
__and__(self, other)     = self & other
__or__(self, other)      = self | other
__xor__(self, other)     = self ^ other
\end{python}


% \begin{description}
% \item[\pymagic{add}(self, other)] entspricht \pyth{self}\verb! + other!
% \item[\pymagic{sub}(self, other)] entspricht \pyth{self}\verb! - other!
% \item[\pymagic{mul}(self, other)] entspricht \pyth{self}\verb! * other!
% \item[\pymagic{truediv}(self, other)] entspricht \pyth{self}\verb! / other!
% \item[\pymagic{floordiv}(self, other)] entspricht \pyth{self}\verb! // other!
% \item[\pymagic{mod}(self, other)] entspricht \pyth{self}\verb! % other!
% \item[\pymagic{divmod}(self, other)] entspricht \pyth{divmod(self, other)}
% \item[\pymagic{pow}(self, other {[, modulo]})] entspricht
%   \pyth{pow(self, other}\verb![, modulo])! bzw.\ \pyth{self}\verb! ** other!
% \item[\pymagic{lshift}(self, other)] entspricht \pyth{self}\verb! << other!
% \item[\pymagic{rshift}(self, other)] entspricht \pyth{self}\verb! >> other!
% \item[\pymagic{and}(self, other)] entspricht \pyth{self}\verb! & other!
% \item[\pymagic{or}(self, other)] entspricht \pyth{self}\verb! | other!
% \item[\pymagic{xor}(self, other)] entspricht \pyth{self}\verb! ^ other!
% \end{description}

\subsubsection{Rightsided Binary Methods} % TODO: Fix translation

Signatur as above.

\dunder{radd},
\dunder{rsub},
\dunder{rmul},
\dunder{rtruediv},
\dunder{rfloordiv},
\dunder{rmod},
\dunder{rdivmod},
\dunder{rpow},
\dunder{rlshift},
\dunder{rrshift},
\dunder{rand},
\dunder{ror},
\dunder{rxor}
\smallskip


Rules:
\begin{python}
self + other == self.__add__(other)
other + self == self.__radd__(other)
\end{python}


\subsubsection{Extended Binary Methods}
Methods, where the object itself is modified.

\dunder{iadd},
\dunder{isub},
\dunder{imul},
\dunder{itruediv},
\dunder{ifloordiv},
\dunder{imod},
\dunder{idivmod},
\dunder{ipow},
\dunder{ilshift},
\dunder{irshift},
\dunder{iand},
\dunder{ior},
\dunder{ixor}
\medskip

Rules:
\begin{python}
self += other == self.__iadd__(other)
\end{python}

\subsection{Methods for Numbers (other Operations)}

\begin{python}
__neg__(self)            = -self
__pos__(self)            = +self
__abs__(self)            = abs(self)
__invert__(self)         = ~self
__complex__(self)        = complex(self)
__int__(self)            = int(self)
__float__(self)          = float(self)
__round__(self [, n])    = round(self [, n])
\end{python}

% \begin{description}
% \item[\pymagic{neg}(self)] entspricht \verb!-!\pyth{self}
% \item[\pymagic{pos}(self)] entspricht \verb!+!\pyth{self}
% \item[\pymagic{abs}(self)] entspricht \pyth{abs(self)}
% \item[\pymagic{invert}(self)] entspricht \pyth{~self}
% \item[\pymagic{complex}(self)] entspricht \pyth{complex(self)}
% \item[\pymagic{int}(self)] entspricht \pyth{int(self)}
% \item[\pymagic{float}(self)] entspricht \pyth{float(self)}
% \item[\pymagic{round}(self {[, n]})] entspricht \pyth{round(self}\verb![,n])!
% \end{description}


\iffalse
\subsection{Instanziierung of a  Class} % TODO: Translation
In C++ wird eine Klasse durch den Konstruktor erzeugt und initalisiert.
Python trennt diese zwei Schritte: die Erzeugung der Klasse
wird über die Methode \pyth{__new__} vorgenommen, während für die
Initialisierung die Methode \pyth{__init__} aufgerufen wird.

\begin{python}
>>> p = Point.__new__(Point)
>>> p
<repr(<__main__.Point at 0x7fc378044f90>)
   failed: AttributeError:
   'Point' object has no attribute '_x'>
>>> Point.__init__(p, x=1, y=2)
>>> p
Point(x=1, y=2)
\end{python}

Üblicherweise wird \pyth{__new__} eher selten benötigt.

\subsection{Creating a Klasse without \pyth{class}}
Die vorige \pyth{Point}-Klasse kann auch über eine klassenlose
Notation erzeugt werden:

\begin{python}
def __init__(self,  x=0, y=0): # s.o.
def __repr__(self): # s.o.

# Signatur: type(name, bases, dict)
Point = type('Point', (object,),
             {'__init__': __init__,
             '__repr__': __repr__, }
            )
\end{python}
\fi

\section{Define Properties via Setter and Getter Methods}
\label{sec.properties}
\emph{Properties} are functions which return, set or delete a
value from an attribute. They look like a normal attribute access,
but internally call a function.

Properties has the following features:

\begin{itemize}
\item Allows to start with a normal attribute
    and extend the code with properties without
    to change the API.
\item Allows to return dynamic calculations.
\item Allows to check allowed values when setting.
\item Prohibit writing to read-only attributes
    (setter remains undefined).
\item Introduced with the \pyth{property} decorator
\end{itemize}

Example of a class \pyth{Kelvin}, which allows only
positive numbers for the temperature.

\begin{python}
class Kelvin:
    def __init__(self, temp):
        # Initialiserung
        self.__temp = temp
    @property
    def temperature(self):
        return self.__temp
    @temperature.setter
    def temperature(self, temp):
        if temp < 0:
            raise ValueError("Kelvin cannot be negative!")
        self.__temp = temp

k1 = Kelvin(10)
k1.temperature # -> 10
k1.temperature = 100 # Ok
k1.temperature = -5 # ValueError
\end{python}


% ------------------------------
\begin{thebibliography}{10}
% Sort lastname author
\bibitem{dbader.classes}
\bauthor{Dan Bader},
\btitle{6 things you’re missing out on by never using classes in your Python code},
\url{https://dbader.org/blog/6-things-youre-missing-out-on-by-never-using-classes-in-your-python-code}

\bibitem{dbader.repr}
\bauthor{Dan Bader},
\btitle{Python String Conversion 101: Why Every Class Needs a \gquote{repr}},
\url{https://dbader.org/blog/python-repr-vs-str}

\bibitem{danjou.python}
\bauthor{Julien Danjou}, 2013,
\btitle{The definitive guide on how to use static, class or abstract methods in Python},
\url{https://julien.danjou.info/guide-python-static-class-abstract-methods}

\bibitem{kettler.magicmethods}
\bauthor{Rafe Kettler}, 2012,
\btitle{A Guide to Python's Magic Methods},
\url{https://rszalski.github.io/magicmethods/}

\bibitem{pypa.pep8}
\bauthor{PyPA},
\btitle{PEP 8 -- Style Guide for Python Code},
\url{https://www.python.org/dev/peps/pep-0008/}

\bibitem{python.formatspec}
\bauthor{Python.org},
\btitle{Format Specification Mini-Language},
\url{https://docs.python.org/3/library/string.html#formatspec}

\bibitem{python.org.glossary}
\bauthor{Python Org},
\btitle{Glossary},
\url{https://docs.python.org/3/glossary.html}

\bibitem{python.specialmethods}
\bauthor{Python.org},
\btitle{Data model -- Special method names},
\url{https://docs.python.org/3/reference/datamodel.html#special-method-names}

\end{thebibliography}
\end{multicols}
\end{document}
