\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}%
\usepackage{cheatsheet}


%opening
\title{Python3 Cheatsheet}
\subtitle{Packaging Python Projects}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}


\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

% \begin{abstract}
\noindent
% At the moment (Nov 2020), the landscape of Python
% packaging is in motion. % Nowadays, there are different methods.
This article focus on packaging with \module{setuptools}.
Expect changes in the future.
% This article shows you the requirements, directory
% structure, files and content needed to package a Python project
% with the \module{setuptools} module.
% \end{abstract}

\section{Terminology}
\label{sec.term}
More terms can be found in the glossary\cite{pypa.glossary}.

\begin{description}
\item[(Built) Distribution]
A format for installing a Python project. It contains all
necessary files for the project and associated metadata (see also Wheel).

\item[Module]
an object that serves as an organizational unit.
Modules have a namespace that can contain any Python objects.
Modules are loaded by importing them.

\item[Package]
A Python module that can contain further submodules.

\item[PEP]
\emph{Python Enhancement Proposal}\cite{pypa.pep}.
PEPs are a collection of technical documents for and from the Python
community to define processes, new features or environments.
Each PEP document has a continuous index and goes
through an extensive discussion process.
Once published, they are not changed anymore.

\item[pip]
Package manager for Python to install, remove, search,
download etc. Often used in combination with virtual environments.

\item[pypi.org]
Web site and repository to help locate and install
Python packages.
Search and installation is done with the \cmd{pip} command.

\item[Requirement File]
Text file which defines dependencies and optionally versions
to other packages.

\item[Source Distribution (or \iqq{sdist})]
A packed archive containing metadata, source files and possibly
documentation to install using \cmd{pip}.

\item[Virtual Python Environment]
An isolated Python environment that allows packages to be
installed as a normal user without affecting system directories.

\item[Wheel]
A ZIP archive with the file extension \filename{.whl} for
distributing Python packages.\cite{pypa.wheel}.
\end{description}


\section{Selecting a License}
\label{sec.license}
Before you write code, consider under which license
your code should be published. The license affects how your
code may be distributed.

The page in \cite{gh.license} gives you a good overview.
Save the license text in the file \filename{LICENSE}.

\section{Finding a Suitable Project Name}
\label{sec.name}
Your project needs a name. To avoid ambiguity and confusion,
proceed as follows:

\begin{enumerate}
\item Choose lower case letters, numbers or underscores for
  your project name. Avoid other characters or even symbols.

\item Search for your name on \url{https://pypi.org} or
  alternatively call \cmd{pip search <PROJECTNAME>}
  (package \pkg{python3-pip}).

  You can skip this step if you do not want to publish your
  project on PyPI.

\item If your chosen project name is already taken,
  use another one and repeat the previous step.
\end{enumerate}


\section{Providing a Source Repository}
\label{sec.gitrepo}
Manage your project with a version control system (VCS).
There are many VCS tools like Git, Mercurial,
Subversion, etc.\ but Git is the most commonly used.

If you have a public open source project using Git,
you can use GitHub, Bitbucket or Gitlab.

\begin{itemize}
\item For a new Git project, create the project on the respective page:

\begin{description}
\item[Bitbucket] \url{https://bitbucket.org/repo/create}
\item[GitHub] \url{https://github.com/new}
\item[Gitlab] \url{https://gitlab.com/projects/new}
\end{description}

Set your project name (see section~\ref{sec.name}) and the owner.
Some providers allow you to set public or private access.
If possible, select \emph{Python} as language for your project
and the license you have chosen.
This will automatically pre-initialize your repository.

\item For an existing repository, check whether the file
  \filename{.gitignore} exists or whether the entries match
  \cite{blau.gitignore,gh.gitignore}.
\end{itemize}


\section{Creating a Directory Structure}
\label{sec.dir}
There are many possibilities for a directory structure.
The following basic structure is based on \cite{maries.pypack}.
It contains the following parts:

\begin{Verbatim}[label={Basic Structure},frame=lines]
<PROJECROOT>/
|-- bin/
|   \-- ...
|-- ChangeLog
|-- devel_requirements.txt
|-- docs/
|   \-- ...
|-- LICENSE
|-- MANIFEST.in
|-- README.rst
|-- requirements.txt
|-- setup.cfg
|-- setup.py
|-- src/
|   \-- <MODULENAME>
|       |-- __about__.py
|       |-- __init__.py
|       \-- ...
\-- tests/
\end{Verbatim}


\begin{description}
\item[\filename{ChangeLog}]
Hand-maintained, reverse chronologically sorted list of all important
changes of a project between single releases or versions.
% Target audience are all \emph{users} of the project.
See \cite{lancan.changelog} for details.

\item[\filename{LICENSE}]
Contains the license text under which the project was published.
See section~\ref{sec.license}.

\item[\filename{*requirements.txt}]
A text file which contains dependency information of required
packages for your project.
Usually there can be several requirement files. 
% In most cases,
% you have one for normal installation (named \filename{requirements.txt})
% and one during development (\filename{devel\_requirements.txt}).
Requirements are installed by the command \cmd{pip}.
See section~\ref{sec.req}.

\item[\filename{README.*}]
Description of the project. Usually in Markdown format (extension \filename{.md}),
Sphinx/Rst (\filename{.rst}) or ASCIIDoc (\filename{.adoc}).
It is usually displayed on the project page.
For a template see \cite{thompson.readme}.

\item[\filename{setup.cfg}]
Optional configuration file used by \filename{setup.py} and other programs.
Usually it contains metadata of the project and other tools.
In this cheatsheet we use it extensively.

\item[\filename{setup.py}]
A required Python script which acts as a \gquote{conductor}.
For more complex setups, it can contain functions to add additional
tasks. For most project, it should be kept simple.

\item[\filename{MANIFEST.in}]
Optional file, which is only needed if additional non-code files (documentation,
images, data, etc.) are to be included in the archive.
Everything that is included or excluded in the \filename{MANIFEST.in}
affects the created archive.

\item[\filename{bin/}]
Directory for your manual scripts (\filename{bin/}).
See section~\ref{sec.script}.

\item[\filename{src/<MODULENAME>/}]
Directory of the project's module. The module directory has to contain
the filename \filename{\_\_init\_\_.py} to make it a module.
You are free to organize and structure it what you need.

\item[\filename{src/<MODULENAME>/\_\_about\_\_.py}]
A Python file which stores some metadata like \field{\_\_version\_\_},
\field{\_\_author\_\_}, and others.
This is not specified by any PEP, so it's purely a convention.
At the minimum, it should contain this line:

\begin{python}
__version__ = "1.1.1"
\end{python}

The \field{\_\_version\_\_} variable is used to fill in the version
referenced in the \filename{setup.cfg} file (see section~\ref{sec.setup.cfg}).
If you prefer to not store your version in a separate file, you
need to adapt the line \verb!version =! in the \filename{setup.cfg} file
and point it to \filename{\_\_init\_\_.py}.

\item[\filename{src/<MODULENAME>/\_\_init\_\_.py}]
A required file to mark directories as regular Python package.
When you import your module, this file is implicitly executed.
All objects therein are bound to names in the package's namespace.
Usually the file can be empty, but is is a good idea to export the modules'
version (imported from \filename{\_\_about\_\_.py}):

\begin{python}
from .__about__ import __version__
\end{python}

\item[\filename{tests/}]
Directory for your test suite. It is possible to create subdirectories
to organize your tests. The pytest framework will find all the files
as long as they named according to the pytest convention (usually
starting with \filename{test\_}).
See section~\ref{sec.test}.
\end{description}


\section{Creating a \filename{setup.py} File}
\label{sec.setup}
A \filename{setup.py} file imports the \module{setuptools}\cite{pypa.setuptools}
module and acts as a conductor. It can perform various tasks,
but in most cases it's required for building and installing the project
and to create an archive (tar, zip, etc.).

In recent years, the trend is to keep the file as clean as possible.
As such, it is recommended to move all relevant metadata into
\filename{setup.cfg} (see section~\ref{sec.setup.cfg}).
This simplifies most projects.
Including additionally functionality into the \filename{setup.py} file
can be useful only for complex setups.

The bare minimum of the file looks like this:

\begin{python}
#!/usr/bin/env python
import setuptools
setuptools.setup()
\end{python}

If you make the file executable, you can run the script as \pyth{./setup.py}.
%Otherwise you must execute the script as follows:
%
%\begin{Verbatim}
%$ python3 setup.py ...
%\end{Verbatim}

\section{Creating a \filename{setup.cfg} File}
\label{sec.setup.cfg}
The file \filename{setup.cfg}\cite{pypa.setuptools.config} stores
the project's metadata.
The configuration file contains the name of the project, the version,
a description and more.

The version is read into with the \verb!attr:! directive. This allows
to avoid duplicate information both in \filename{setup.cfg} and the
module.
The line \verb!version! in \filename{setup.cfg} points to a file and
a variable name.
Replace the placeholders with the appropriate information.

\begin{inicode}
[metadata]
name = <PROJECTNAME>
version = attr:<MODULENAME>.__about__.__version__
description = "..."
long_description = file: README.rst
long_description_content_type = text/x-rst
author = Tux Penguin
author_email = tux@greeland.com
# maintainer = ...
# maintainer_email = ...
url = https://github.com/tux/foo
download_url = https://github.com/tux/foo/downloads
classifiers =
  # See https://pypi.org/pypi?%3Aaction=list_classifiers
license = MIT

[options]
package_dir =
    =src
packages = find:
python_requires = >=3.6.*
include_package_data = True

[options.packages.find]
where = src
\end{inicode}

\section{Creating a Script}
\label{sec.script}
Usually a Python project contains scripts to allow the user
interact with the project.
The \module{setuptools} module knows two ways
how scripts are referenced within \filename{setup.cfg}:

\begin{itemize}
\item Directly via the \field{scripts} keyword.

Write the scripts and store them in the \filename{bin/} directory.
Add the following content to the \filename{setup.cfg} file:

\begin{inicode}
[options]
scripts =
  bin/foo.py
\end{inicode}

\item Indirectly via the \field{entry\_points} keyword.

In this scenario, the script is automatically created by the
\module{setuptools} module.
The module must have a function that can handle command line arguments.
Let's assume that the script has the following structure:

\begin{Verbatim}
src/
  \-- foo/
      \-- cli.py
\end{Verbatim}

If the code contains a \func{main()} function in the \module{foo.cli} module,
then the following entry in \filename{setup.cfg} automatically
creates the script \filename{bar}:

\begin{inicode}
[options.entry_points]
console_scripts =
  bar = foo.cli:main
\end{inicode}

The \field{console\_scripts} key follows this syntax:

\begin{Verbatim}
<SCRIPTNAME> = <MODULEPATH>:<FUNCTION>
\end{Verbatim}

The placeholders mean:

\begin{description}
\item [\field{SKRIPTNAME}] the script name to be created
  (in our example \ops{bar}).

\item [\field{MODULEPATH}] the name of the module
  in dot notation (in our example \ops{foo.cli}).

\item[\field{FUNCTION}] the name of the function within
  the module \emph{MODULEPATH} (in our example \ops{main}).
\end{description}
\end{itemize}


\section{Defining Dependencies}
\label{sec.req}
% https://stackoverflow.com/questions/43658870/requirements-txt-vs-setup-py
The Python project might require other Python projects to run
correctly. Moreover, during development you need additional
packages. All these requirements can be handled by the \module{setuptools}
module.

The \module{setuptools} module distinguishes between two
requirements types:

\begin{description}
\item[Runtime Dependencies (abstract)]
  are used to define packages that are needed to run and operate the
  \emph{single project}.
  These dependencies are added in the file \filename{setup.cfg}.
  Usually the key contains a minimal list.

\item[Development Dependencies (concrete)]
  are used to define packages needed when developing the project.
  They usually define \emph{complete Python environment}.
  These dependencies are added in requirement files.
  The dependencies are usually exhaustive and pinned
  to a specific version to achieve repeatable builds.
\end{description}

Both \filename{setup.cfg} and requirement file has the same syntax for
dependencies.
Every dependency is added on a separate line and contains the package
name and an optional version specifier.

\subsection{Defining Runtime Dependencies}

To define the runtime requirements for the project, add the following
lines in file \filename{setup.cfg}:

\begin{inicode}
[options]
install_requires =
  lxml>=4.0
  requests
\end{inicode}

In this example, the project requires the two packages
\module{lxml} and \module{requests}. The \module{lxml} package
requires version 4.0 or higher whereas for \module{requests} any
version is sufficient.

\subsection{Defining Development Dependencies}
To define required dependencies, add the package list in a requirement file.

To avoid duplicating work, add a \filename{requirements.txt}
file in the project root with the following content
(the hash character is a comment):

\begin{Verbatim}
# requirements.txt
-e .
\end{Verbatim}

The \option{-e} (\gquote{editable mode}) option installs a project
in \iqq{develop mode}: it installs the project from the
local path. It also takes into account all dependencies stored in
\filename{setup.cfg}. In \iqq{develop mode}, you can change the source
code and it directly affects the installation.

However, this does not solve additional dependencies for development.
To add these dependencies, you \gquote{chain} requirement files.

In another requirement file (for example, \filename{devel\_requirements.txt})
add a reference to \filename{requirements.txt} like so:

\begin{Verbatim}
# devel_requirements.txt
-r requirements.txt
wheel
pytest
flake8
# ... more entries
\end{Verbatim}

If the file \filename{devel\_requirements.txt} gets installed via
\cmd{pip} the command will install all dependencies from both
requirement files (see section~\ref{sec.venv}).


\section{Creating a Virtual Python Environment}
\label{sec.venv}
A virtual Python environmentt\cite{real.pyvenv} can be used
for developing and testing projects, as it offers the
following advantages:

\begin{itemize}
\item Create an environment for Python projects only. So
  each project has its own customized environment.
\item Isolate the directory from the system. This allows
  you to have different versions in both places without
  causing version conflicts.
\item Help with testing by creating a consistent environment.
\item Work without administrator rights.
\end{itemize}

To create a virtual Python environment for the first time
proceed as follows:

\begin{enumerate}
\item Create a virtual Python environment in the directory
  \filename{.env}:
\begin{Verbatim}
$ python3 -m venv .env
\end{Verbatim}

\item\label{st.activate} Activate the virtual Python environment:
\begin{Verbatim}
$ source .env/bin/activate
\end{Verbatim}

When the viritual Python environment is activated, the prompt
changes and you see \texttt{(.env)} at the beginning.

\item Optionally update \cmd{pip} and \cmd{setuptools}:
\begin{Verbatim}
(.env) $ pip install -U pip setuptools
\end{Verbatim}

\item Install all development dependencies (section~\ref{sec.req}):
\begin{Verbatim}
(.env) $ pip install -r devel_requirements.txt
\end{Verbatim}

\item Check the list of installed packages:
\begin{Verbatim}
(.env) $ pip list
foo (1.1.1, /home/tux/repos/foo)
# ...
\end{Verbatim}

If you see a path after the version number of the project,
you have done everything right.
\end{enumerate}

To undo the activation, call \cmd{deactivate}.

If you created scripts using the \field{entry\_points} or \field{scripts}
keywords (see section~\ref{sec.script}), find the scripts under
 \filename{.env/bin}.

\section{Documenting the Project}
\label{sec.doc}
For Python projects, Sphinx\cite{brandl.sphinx} is a good choice.

To create a basic structure, proceed as follows:

\begin{enumerate}
\item If you haven't done so, activate the virtual Python environment
  (see section~\ref{sec.venv}):

\begin{Verbatim}
$ source .env/bin/activate
\end{Verbatim}

\item Create the file \filename{docs/requirements.txt} and add the
  following line:

\begin{Verbatim}
sphinx
\end{Verbatim}

\item Add the following line in \filename{devel\_requirements.txt}:

\begin{Verbatim}
-r docs/requirements.txt
\end{Verbatim}

\item Install all dependencies (this includes also dependencies
  for the documentation):
\begin{Verbatim}
(.env) $ pip install -r requirements.txt
\end{Verbatim}

\item Call the tool \cmd{sphinx-quickstart}:
\begin{Verbatim}
(.env) $ sphinx-quickstart --no-batchfile \
   --ext-githubpages --ext-todo  docs/
\end{Verbatim}

The tool asks some more questions.
It is sufficient to press the Enter key to accept the default value.

\item Create a first draft in HTML:
\begin{Verbatim}
(.env) $ make -C doc html
\end{Verbatim}

Find the documentation under the directory
\url{PROJECTDIR/_build/html/index.html}.
\end{enumerate}

Add then documentation in the ReST format.

\section{Testing the Project}
\label{sec.test}
Although there is a module for testing in the standard library
(\module{unittest}), pytest\cite{krekel.pytest} is the
simpler and more powerful tool.
Tests are written via \pyth{assert} and need
no class as in \module{unittest}.

To integrate tests into the project proceed as follows:

\begin{enumerate}
\item Create a directory \filename{tests} in the project directory.

\item Make the following changes to the \filename{setup.cfg} file:

\begin{enumerate}
  \item Add requirements:
\begin{inicode}
[options]
setup_requires = pytest-runner
tests_require =
  pytest
  pytest-cov
\end{inicode}

    \item Add configuration. Replace \verb!<MODULENAME>!:
\begin{inicode}
[tool:pytest]
norecursedirs = build/ .git/ .env/ env/ .tmp/ .tox/
testpaths = tests docs
addopts =
    --ignore=build/
    --ignore=.git/
    --ignore=.env/
    --ignore=env/
    --ignore=.tmp/
    --ignore=.tox/
    --no-cov-on-fail
    --cov=<MODULENAME>
    --cov-report=term-missing
\end{inicode}
\end{enumerate}


\item Check if the following lines are present in
  \filename{devel\_requirements.txt}:
\begin{Verbatim}
pytest
pytest-cov
\end{Verbatim}

\item Write tests\cite{krekel.pytest}
Save each file in the directory \filename{tests} and begin
the file name with \filename{tests\_}.

\item Run \cmd{pytest} the test suite:
\begin{Verbatim}
(.env) $ pytest -v
...
====== 12 passed, 2 skipped in 0.86 seconds =======
\end{Verbatim}
\end{enumerate}

\section{Deploying the Project}
\label{sec.archive}
To make it easier for users to find, install and use your project
you need to distribute it. Distribute source and built distribution:

\begin{itemize}
\item Source Distribution.

All relevant files are transferred to an archive.
Which files are relevant defines \filename{setup.cfg} and
\filename{MANIFEST.in}.

Available formats can be listed as follows:

\begin{Verbatim}
(.env) $ setup.py sdist --help-formats
\end{Verbatim}

To create a source distribution in ZIP format, run:

\begin{Verbatim}
(.env) $ setup.py sdist --formats=zip,bztar
\end{Verbatim}

Add the desired formats to the \filename{setup.cfg} so you
do not have to specify \option{--formats} every time:

\begin{inicode}
[sdist]
formats=zip,bztar
\end{inicode}

Find the file(s) in the directory \filename{dist/}.

\item Built Distribution.

A file with the extension \filename{.whl} which
already contains all relevant files in an installable form.

\begin{Verbatim}
(.env) $ setup.py bdist_wheel
\end{Verbatim}

You can also find the file in the directory \filename{dist/}.

To create a wheel file in a virtual Python environment
install, execute:

\begin{Verbatim}
(.env) $ pip install foo-1.1.1-*.whl
Processing foo-1.1.1-py3-none-any.whl
Collecting ...
Installing collected packages: ...
Successfully installed ...
\end{Verbatim}
\end{itemize}


\section{Publishing on PyPI}
\label{sec.pypi}
Once you have the project as a distribution package
(see section~\ref{sec.archive}) you can publish it on PyPI.

PyPI is \emph{the} central place for all Python packages.

This step is only necessary if you want to share the project
with the Python community.
Use \cmd{twine}\cite{pypa.twine} to publish it:

\begin{enumerate}
\item Register in \url{https://pypi.org} and \url{https://test.pypi.org}.
  The latter is a test instance to check the upload of the packages and
  the appearance of the README.
\item Install \cmd{twine} in the virtual Python environment:
\begin{Verbatim}
(.env) $ pip install twine
\end{Verbatim}

\item Optionally add \cmd{twine} to the file \filename{devel\_requirements.txt}.

\item If present, delete the directory \filename{dist}.

\item Build a wheel, \filename{.zip}, and \filename{.tar.bz2} file
(see section~\ref{sec.archive}).

\item Check the distribution files before uploading:
\begin{Verbatim}
(.env) $ twine check dist/
\end{Verbatim}

\item Test the project by uploading it to the test server
(you will be asked for a username and a password):

\begin{Verbatim}
(.env) $ twine upload \
   --repository-url https://test.pypi.org/legacy/ \
   dist/*
username: ...
password: ...
\end{Verbatim}

\item Try installing the project in a different(!) virtual Python environment
  using the test server:

\begin{Verbatim}
$ mkdir /tmp/test-PROJECTNAME
$ cd /tmp/test-PROJECTNAME
$ python3 -m venv .env && source .env/bin/activate
(.env) $ pip install \
  --index-url https://test.pypi.org/legacy/ \
  PROJEKTNAME
\end{Verbatim}

\item If everything is correct, upload the project to the official
  PyPI server:

\begin{Verbatim}
(.env) $ twine upload dist/*
\end{Verbatim}
\end{enumerate}

If you prefer not to enter username and password all the time,
save your username and password in a keyring.
See \url{https://pypi.org/project/keyring/} for more information.


\section{Additional Helpers}
The following subsections shows some helpful adjustments to the
already existing project.

\subsection{Updating the Version Consistently}
Version information is often available in many different files
(\filename{setup.py}, \filename{\_\_init\_\_.py},
documentation etc.) To manage this information consistently,
use the package \module{bump2version}\cite{gh.bump2version}.
It is based on \emph{Semantic Versioning}\cite{werner.semver}.

To use the \module{bump2version} package for the project,
proceed as follows:

\begin{enumerate}
\item Add the following line to \filename{devel\_requirements.txt}:

\begin{Verbatim}
bump2version
\end{Verbatim}

\item Install the development requirements:
\begin{Verbatim}
(.env) $ pip install -r devel_requirements.txt
\end{Verbatim}

\item In the file \filename{setup.cfg}:

\begin{enumerate}
  \item Add the current version (line with \verb!current_version!)
    and other options:
\begin{inicode}
[bumpversion]
current_version = 1.1.1
commit = True
tag = False
\end{inicode}

  \item For every file that contains a version information, add
  a section \verb![bumpversion:file:<FILE>]!. For example,
  the Sphinx documentation configuration file \filename{docs/conf.py}
  contains version information. To make this file checked by
  \module{bump2version}, add this line:

\begin{inicode}
[bumpversion:file:docs/conf.py]
\end{inicode}
\end{enumerate}

\item Make sure that status of your local repository does not contain
  modified files (use \cmd{git status} for example).

\item Increase the version (major, minor or patch).

  For example, to go from 0.1.0 to 0.2.0 increase the minor part
  with the following command:

\begin{Verbatim}
(.env) $ bump2version minor
\end{Verbatim}
\end{enumerate}

This updates the version in all files you have included in the
configuration file.

\subsection{Checking the Code Style}
\label{sec.pycodestyle}
Check the code for conformity to PEP8 with
\cmd{pycodestyle}\cite{pycqa.pycodestyle}.
For this purpose, the following settings must be made in the
file \filename{setup.cfg}:

\begin{inicode}
# setup.cfg
[pycodestyle]
max-line-length = 80
statistics = True
show-source = True
exclude = .env/*,docs/*,tests/*,build/*
# maybe further options
\end{inicode}

Execute the following command to get an overview of the project:

\begin{Verbatim}
(.env) $ pycodestyle src
\end{Verbatim}


\section{Alternatives}
If you prefer an automated alternative, consider the
project \emph{cookiecutter} by Audrey Roy Greenfeld\cite{greenfeld.cookiecutter}.

It contains a large number of templates that you can use to create
projects consistently in Python or any other language.


\begin{thebibliography}{10}
% Sort lastname author
\bibitem{blau.gitignore}
\bauthor{Joe Blau},
\btitle{Create useful .gitignore files for your project},
\url{https://www.gitignore.io}

\bibitem{brandl.sphinx}
\bauthor{Georg Brandl u.~a.},
\btitle{Sphinx -- Python Documentation Generator},
\url{http://www.sphinx-doc.org}

\bibitem{lancan.changelog}
\bauthor{Olivier Lacan},
\btitle{Keep a changelog},
\url{https://keepachangelog.com/en/1.0.0/}

\bibitem{gh.license}
\bauthor{GitHub Inc.},
\btitle{Choose an open source license},
\url{https://choosealicense.com}

\bibitem{gh.gitignore}
\bauthor{GitHub Inc.},
\btitle{Python.gitignore},
\url{https://github.com/github/gitignore/blob/master/Python.gitignore}

\bibitem{greenfeld.cookiecutter}
\bauthor{Audrey Roy Greenfeld},
\btitle{Cookiecutter},
\url{https://github.com/audreyr/cookiecutter}

\bibitem{maries.pypack}
\bauthor{Ionel Cristian Mărieș},
\btitle{Packaging a python library},
\url{https://blog.ionelmc.ro/2014/05/25/python-packaging/}

\bibitem{mavrakis.pypack}
\bauthor{Nick Mavrakis},
\btitle{A tour on Python Packaging},
\url{https://manikos.github.io/a-tour-on-python-packaging}

\bibitem{krekel.pytest}
\bauthor{Holger Krekel u.a.},
\btitle{Pytest},
\url{https://docs.pytest.org}

\bibitem{pycqa.pycodestyle}
\bauthor{Python Code Quality Authority},
\btitle{pycodestyle -- Python style guide checker},
\url{https://pypi.org/project/pycodestyle}

\bibitem{pypa.glossary}
\bauthor{PyPA},
\btitle{Glossary},
\url{https://packaging.python.org/glossary}

\bibitem{pypa.setuptools.config}
\bauthor{PyPA},
\btitle{Configuring setup() using setup.cfg files}
\url{https://setuptools.readthedocs.io/en/latest/userguide/declarative_config.html}

\bibitem{pypa.setuptools}
\bauthor{PyPA},
\btitle{Setuptools},
% \url{https://docs.python.org/3.6/distutils/setupscript.html#additional-meta-data}.
\url{https://setuptools.readthedocs.io/en/latest/setuptools.html#new-and-changed-setup-keywords}

\bibitem{pypa.sample}
\bauthor{PyPA},
\btitle{A sample Python project},
\url{https://github.com/pypa/sampleproject}

\bibitem{pypa.pep}
\bauthor{PyPA},
\btitle{PEP 0 -- Index of Python Enhancement Proposals (PEPs)},
\url{https://www.python.org/dev/peps/}

\bibitem{pypa.twine}
\bauthor{PyPA},
\btitle{Twine -- Collection of utilities for publishing packages on PyPI},
\url{https://pypi.org/project/twine}

\bibitem{pypa.wheel}
\bauthor{PyPA},
\btitle{wheel},
\url{https://wheel.readthedocs.io/en/stable}

\bibitem{real.pyvenv}
\bauthor{Real Python},
\btitle{Python Virtual Environments: A Primer},
\url{https://realpython.com/python-virtual-environments-a-primer}

\bibitem{schraitle.pythonsample}
\bauthor{Thomas Schraitle},
\btitle{Python Sample Project},
\url{https://github.com/tomschr/python-sample}


\bibitem{thompson.readme}
\bauthor{Billie Thompson},
\btitle{README-Template.md},
\url{https://gist.github.com/PurpleBooth/109311bb0361f32d87a2}

\bibitem{gh.bump2version}
\bauthor{Christian Verkerk},
\btitle{bump2version -- Version-bump your software with a single command},
\url{https://github.com/c4urself/bump2version}

\bibitem{werner.semver}
\bauthor{Tom Preston-Werner},
\btitle{Semantic Versioning 2.0.0},
\url{https://semver.org}
\end{thebibliography}

\end{multicols}
\end{document}
