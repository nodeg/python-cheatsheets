%
\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

\usepackage[simplified]{pgf-umlcd}

%opening
\title{Python3 Cheatsheet}
\subtitle{Functions}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Introduction}\label{intro}
Functions are \gquote{first class objects}\cite[page 141]{ramalho}

\begin{itemize}
    \item Created at runtime
    \item Assigned to a variable or element in a data structure
    \item Passed as an argument to a function
    \item Returned as the result of a function
\end{itemize}

\section{Terminology}
Definitions taken from \cite{python.org.glossary}:

\begin{description}
\item[Argument] 
    A value passed to a function (or method) when calling the function.
    See section~\ref{sec.func.argtypes}, page \pageref{sec.func.argtypes}.
\item[Annotation]
    A label associated with a variable, a class attribute or a function
    parameter or return value, used by convention as a type hint.
    See special attribute \pymagic{annotation} of the object.
\item[Function]
    A series of statements which returns some value to a caller.
\item[Generator]
    A function which returns a generator iterator. The function contains
    a \pyth{yield} expression.
\item[Generator iterator]
    An object created by a generator function.
\item[Lambda]
    An anonymous inline function consisting of a single expression which
    is evaluated when the function is called.
    See section \ref{sec.lambda}, page~\pageref{sec.lambda}.
\item[Parameter]
    A named entity in a function (or method) definition that specifies an
    argument (or in some cases, arguments) that the function can accept.
\item[Type hint]
    An annotation that specifies the expected type for a variable, a
    class attribute, or a function parameter or return value.
% \item[] 
\end{description}

% TODO: Really needed?
% \iffalse
\section{Function Definition}
According to \cite{python.org.func}, the following function defines a
user-specified function object:

\begin{verbatim}
funcdef  ::=  [decorators] "def" funcname 
              "(" [parameter_list] ")"
              ["->" expression] ":" suite
\end{verbatim}
% \fi

\section{Built-in Functions}
% abs, any, all,
% ascii, bin, callable, chr, compile, delattr, dir, display, divmod, enumerate
% eval, exec, format, getattr, globals, hasattr, hash, hex, 

\url{https://docs.python.org/3/library/functions.html}


\section{Functions with Docstrings}\label{sec.docstrings}
Whenever you write a new package or function, consider documenting it with
a \emph{docstring}.
A docstring is \gquote{a string literal which appears as the first expression
in a class, function or module}.\cite{python.org.glossary}

A docstring is \emph{not} a comment.

For example, the \module{fractions} module has the function \func{gcd}
(greatest common divisor), which contains the following docstring:

\begin{python}
def gcd(a, b):
    """
    Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b
    (so that when b is divided by it, the result comes
    out positive).
    """
\end{python}

\subsection{Displaying Help}
In an interactive Python shell session, you can display the
docstring with the \func{help} function:

\begin{python}
# run: python3 or ipython3
>>> from fractions import gcd
>>> help(gcd)
# output the same as above
\end{python}

The docstring is stored in the \pymagic{doc} attribute
of the object:

\begin{python}
>>> gcd.__doc_
'Calculate the Greatest Common Divisor...'
\end{python}


\subsection{Knowing Docstring Formats}\label{sec.docstring.formats}
Some projects document their parameters, attributes, exceptions, 
return values, and deprecation notices in the docstring too. 
Over time, a number of formats has been established:

\begin{description}
    \item[Google style] used by their own styleguide, see \cite{google.styleguide}.
    \item[NumPy style] used by the NumPy community (a package for scientific computing), see \cite{numpydoc}.
    \item[ReST style] used by Sphinx (a document generator), see \cite{sphinx}.
\end{description}

Use which formats that you feel most comfortable with. It doesn't matter which one you
use as long you use it. Adding such information to your function and classes
improves not only the code, but can be used to (auto)generate API documentation easily.

Here is a docstring example in ReST style:

\begin{python}
"""[Summary]

:param [ParamName]: [ParamDescription],
    defaults to [DefaultParamVal]
:type [ParamName]: [ParamType](, optional)
...
:raises [ErrorType]: [ErrorDescription]
...
:return: [ReturnDescription]
:rtype: [ReturnType]
\end{python}

If you use type hints as discussed in section~\ref{sec.typehints}, you can avoid
the \verb!:type:! and \verb!:rtype:! directives.


\section{Functions with Doctests}\label{sec.doctests}
For \emph{easy} functions, a doctest acts as a documentation
example and a test case.
A doctest looks like an interactive Python session.\cite{python.org.doctest}
To mark a line as doctest, add \verb!>>>!, a space, the function and its
arguments. The next line contains the expected result (without \verb!>>>!).

The following function adds two integers (not very fancy, but
for demonstrating):

\begin{python}
def add(a, b):
    """Adds two integers when both are >= 0.

    >>> add(1, 2)
    3
    >>> add(1, -2)
    Traceback (most recent call last):
      ...
    ValueError: Both arguments have to be positive
    """
    if a < 0 or b < 0:
        msg = "Both arguments have to be positive"
        raise ValueError(msg)
    return a + b
\end{python}


To check if all tests are correct, call the script as follows
(assuming a file \filename{add.py}):

\begin{verbatim}
$ python3 -m doctest -v add.py
Trying:
    add(1, 2)
Expecting:
    3
ok
Trying:
    add(1, -2)
Expecting:
    Traceback (most recent call last):
      ...
    ValueError: Both arguments have to be positive
ok
1 items had no tests:
    __main__
1 items passed all tests:
   2 tests in __main__.add
2 tests in 2 items.
2 passed and 0 failed.
Test passed.
\end{verbatim}

\section{Function Argument Types}\label{sec.func.argtypes}
Python signatures can be quite versatile in regards how
arguments can be passed to the function.

For example, the built-in function \func{complex} has
the following docstring:

\begin{verbatim}
complex(real[, imag]) -> complex number
\end{verbatim}

As Python distinguish between
two kind of arguments, you can call \func{complex}
in two ways:

\begin{description}
\item[Positional arguments]
Appears at a certain position. Positional arguments can
only appear at the beginning of an argument list:

\begin{python}
complex(3, 5)
\end{python}

\item[Keyword argument]
Appears either after positonal arguments or as a
name-value pairs:

\begin{python}
complex(real=3, imag=5)
\end{python}
\end{description}

As you could see, the \func{complex} function could be
called in two ways. It's a matter of taste what you use.
For complexer functions, the keyword arguments would
be easier to read (but more to type).

\subsection{Accepting One Required Argument}
Not a big issue: Just give the argument a name:

\begin{python}
def hello(name):
    return f"Hello {name}"
\end{python}

\subsection{Accepting Default Arguments}
Give the argument a name and a value:

\begin{python}
def hello(name="Tux"):
    return f"Hello {name}"

>>> hello()
'Hello Tux'
>>> hello("Geeko")
'Hello Geeko'
\end{python}
    
You can have as many default arguments as you want. However,
they can only appear after required arguments. This signature
would not be possible:

\begin{python}
>>> def foo(a, b=0, c): 
...     pass
def foo(a, b=0, c):
           ^
SyntaxError: non-default argument follows default argument
\end{python}

Furthermore, be careful when you use mutable objects as default
arguments. You can get unexpected results:

\begin{python}
>>> def appenditem(item, thelist=[]): 
...     thelist.append(item) 
...     return thelist
>>> appenditem(2)                                                                                                                          
[2]
>>> appenditem(3)                                                                                                                          
[2, 3]
>>> appenditem(4)
[2, 3, 4]
\end{python}

\subsection{Accepting Any Number of Arguments}
This is a \emph{var-positional} argument.
Use a star in front of the argument name to capture
an arbitrary sequence of positional arguments.
The function gets passed a \verb|tuple| object.
Sometimes you see the name \verb|*args| as argument
name.

\begin{python}
def hello(*names):  # names is a tuple object
    return "Hello {}!".format(
        ", and ".join([", ".join(names[:-1]),
             names[-1]]))

>>> hello("Tux", "Wilber", "Geeko")
'Hello Tux, Wilber, and Geeko!'
\end{python}

\subsection{Accepting Positional-Only Arguments}
This feature was introduced in Python 3.8, so don't use it in
previous versions.
Include a \verb|/| character in the parameter list of the
function definition after them.

It forces the caller to use positional arguments. Passing
arguments as keywords are not allowed:

\begin{python}
def hello(name, age, /):
   return f"Hello {name} ({age})"

>>> hello("Tux", 40)
'Hello Tux (40)'
>>> hello(name="Geeko", age=20)
Traceback (most recent call last):
  ...
TypeError: hello() got some positional-only arguments
  passed as keyword arguments: 'name, age'
\end{python}  

\subsection{Accepting Abitrary Keyword Arguments}
This is a \emph{var-keyword} argument.
Use two star \verb|**| characters in front of the argument
name to capture arbitrary keyword arguments.
The function gets passed a \verb|dict| object.
Sometimes you see the name \verb|**kwargs| as argument name:

\begin{python}
def person(**kwargs): # kwargs is a dict object
    if kwargs.get("name"):
        print(f"You are {name}")
    ...

>>> person(x=2)
>>> person(name="Tux")
'You are Tux'
\end{python}

\subsection{Accepting Keyword-Only Arguments}
Include a single \verb|*| character in the parameter list of
of the function definition before them.
Keyword-only arguments brings you these benefits:

\begin{itemize}
\item Makes the calling code more explicit, leading 
    to greater code clarity. Make code also more verbose.
\item Allows you to add more keyword arguments.
\end{itemize}

\begin{python}
def greeting(name, *, use_colors):
    ...

>>> greeting("Tux")
>>> greeting("Tux", use_colors=True)
>>> greeting("Tux", True)
Traceback (most recent call last):
  ...
TypeError: greeting() takes 1 positional argument
  but 2 were given
\end{python}


\subsection{Returning Multiple Values}
To return multiple values from a function, separate each
value by commas:

\begin{python}
def func():
    return 1, 2, 3, 4
\end{python}

Actually, Python creates a \verb|tuple| object, storing each
value:

\begin{python}
>>> result = func()
>>> type(result)
tuple
>>> result[2]
3
\end{python}

\subsection{Unpacking Arguments to Functions}
For positional arguments more than one, you can also pass a tuple
or a list.
However, you need to \gquote{unpack} it with the star notation
as you can see here:

\begin{python}
>>> def add(a, b, c):
...     return a + b + c
>>> myargs = (1, 2, 3)
>>> add(*myargs)
6
\end{python}

Keep in mind, you cannot have more elements in your tuple/list than
the required amount of arguments.

Same can be done with keyword arguments, but with the two star
notation:

\begin{python}
>>> mykwargs = dict(a=1, b=2, c=3)
>>> add(**mykwargs)
6
\end{python}

Keep in mind, you cannot have unknown keys in your dictionary
that doesn't match to your argument names.


\section{Function Annotations and Type Hints}\label{sec.typehints}
A function annotation is \gquote{a label associated with a variable,
a class attribute or a function parameter or return value,
used by convention as a type hint.}\cite{python.org.glossary}

For example, you can add \emph{type hints} to mark the input and
output types of a function:

\begin{python}
def add(a: int, b: int) -> int:
    return a + b
\end{python}

For more complex types, use the \module{typing}\cite{python.org.typing} module.

Type hints are \emph{optional}, they are \emph{not}
enforced by Python nor checked at runtime.

To access the type hint information, Python stores them
in the \pymagic{annotation} attribute:

\begin{python}
>>> add.__annotations__                                                                                                                 
{'a': int, 'b': int, 'return': int}
\end{python}

To check type hints, use a type checker\cite{hjelle.typechecking}
like \cmd{mypy}, \cmd{pytype}, \cmd{pyright}, and others.
This is probably a task for a continuous integration
service.

\section{Function Argument Introspection}
Every function object contains the attribute \pymagic{code}.
It is further divided into attributes which gives you the
number of arguments (\attr{co\_argcount}), the name of the
function (\attr{co\_name}), and their argument names
(\attr{co\_varnames}).
More attributes can be seen in \cite{python.org.inspect}.

\begin{python}
def spam(a, b=42):
    ...
>>> spam.__code__.co_argcount
2
>>> spam.__code__.co_name
'spam'
>>> spam.__code__.co_varnames
('a', 'b')
\end{python}

\section{Argument Freezing} % with \func{functools.partial}
The function \func{functools.partial} is used to \gquote{freeze}
some portions of a function's arguments.

For example, the built-in function \func{int} allows to pass a base
keyword argument. By default, it set to 10, meaning we are
using decimal values. To change this to two (for binary),
you could \gquote{freeze} the \verb!base! argument:

\begin{python}
>>> from functools import partial
>>> basetwo = partial(int, base=2)
>>> basetwo.__doc__ = 'Convert base 2 string to an int.'
>>> basetwo('10010')
18
\end{python}

\section{Anonymous Functions (Lambdas)}\label{sec.lambda}
Anonymous functions, lambda expressions, lambda functions etc.
are used interchangeably in this cheatsheet.
Lambda expressions are functions created using the following syntax:

\begin{python}
lambda [parameters]: expression    
\end{python}

The \verb!parameters! are optional. If supplied, they are comma-separated
variable names (positional arguments).

A lambda expression is composed of:

\begin{itemize}
    \item the keyword \pyth{lambda}
    \item a bound variable
    \item the body
\end{itemize}

It returns a function object. To add one to an argument,
you can write this lambda expression:

\begin{python}
>>> lambda x: x + 1
<function __main__.<lambda>(x)>
\end{python}

As it is function, it can be called with an argument:

\begin{python}
>>> (lambda x: x + 1)(2)
3
\end{python}

The above lambda function is equivalent to writing this:

\begin{python}
def add_one(x):
    return x + 1
\end{python}

Lambdas are usually added in \pyth{sorted()} to influence the
sorting algorithm like in this example:

\begin{python}
>>> ids = ['id1', 'id2', 'id30', 'id3', 'id22', 'id100']
>>> sorted(ids) # Lexicographic sort
['id1', 'id100', 'id2', 'id22', 'id3', 'id30']
>>> sorted(ids, key=lambda x: int(x[2:]))
['id1', 'id2', 'id3', 'id22', 'id30', 'id100']
\end{python}


\section{Generators}
\iffalse
\gquote{A function which returns a generator iterator.
It looks like a normal function except that it contains
yield expressions for producing a series of values usable
in a for-loop or that can be retrieved one at a time with
the next() function.}\cite[generator]{python.org.glossary}
\fi

A generator is in Python:

\begin{itemize}
    \item a function which state is remembered.
    \item contains a \pyth{yield} expression.
    \item returns a \emph{generator iterator} (kind of \gquote{lazy iterator}).
    \item can be used in for-loops.
\end{itemize}

You can define a generator like this:

\begin{python}
def hello(name1, name2):
    yield f"Hello {name1}"
    yield f"Hello {name2}"
\end{python}

To run it, you can use it in a for-loop:

\begin{python}
>>> for result in hello("Tux", "Geeko"):
...    print(result)
Hello Tux
Hello Geeko
\end{python} 

It is possible to get the values \gquote{manually} with the
built-in function \pyth{next()}:

\begin{python}
>>> gen = hello("Tux", "Geeko")
>>> next(gen)
Hello Tux
>>> next(gen)
Hello Geeko
\end{python} 

If the generator is exhausted, you will get an exception:

\begin{python}
>>> next(gen)
Traceback (most recent call last)
...
StopIteration
\end{python} 

% \section{Encapsulating Inner Functions}

\section{Coroutines}
A function that deliberately yield control over to the caller,
but does not end its context in the process, instead maintaining
it in an idle state.

% \subsection{Types of Coroutines}
Python distinguish two types of coroutines:

\begin{description}
    \item[Simple Coroutines] that is what we describe here.
    \item[Native Coroutines] also called \gquote{asyncronous coroutines}
        are used in concurrency code and the \module{asyncio} module.
\end{description}

% \subsection{Generators vs. Coroutines}
The main difference between generators and coroutines are:

\begin{itemize}
    \item generators \emph{produce} data.
        They \emph{pull} data through iteration.
    \item coroutines \emph{consume} data.
        They \emph{push} data with \pyth{.send()}.
\end{itemize}

% \subsection{Parts of a Coroutine}
A coroutine is in Python:

\begin{itemize}
    \item a function
    % \item contains usually a \pyth{while} instruction.
    \item uses a \pyth{yield} instruction on the \emph{right} side to consume data.
    \item needs a \pyth{.send()} method outside of the coroutine to send the data
        into the coroutine
    \item has to be \gquote{primed} (with \pyth{next(generator)})
\end{itemize}

\begin{python}
def averager():
    total = 0.0
    count = 0
    average = None
    while True:
        term = (yield average)
        total += term
        count += 1
        average = total / count
\end{python}

\begin{python}
>>> coro = averager()
>>> # priming the coroutine, advance to first yield
>>> next(coro)
>>> coro.send(10)
10.0
>>> coro.send(20)
20.0
>>> coro.send(5)
15.0
\end{python}


\subsection{Terminating a Coroutine}
Use the \pyth{.throw()} method to send an exception to
the coroutine.
Exception originates at the \pyth{yield} expression.

\begin{python}
>>> coro.throw(ValueError, "Oh no!")
Traceback (most recent call last)
  ...
ValueError: Oh no!
\end{python}


\subsection{Closing a Coroutine}
A coroutine might run indefinitley. Use the \pyth{.close()}
method to shut it down:

\begin{python}
>>> coro.close()
>>> coro.send(2)
Traceback (most recent call last)
  ...
StopIteration:
\end{python}

To handle the situation when a coroutine is closed, you
need to catch the \pyth{StopIteration} exception inside:

\begin{python}
def mycoroutine():
    try:
        while True:
            item = (yield)  # Receive an item
            ...
    except StopIteration:
        # Done
        ...
\end{python}

\section{Closures}
% A \emph{closure} in Python is a \gquote{method of
% binding data to a function without actually passing
% them as parameters}.

A \emph{closure} is a nested function with access to free variables
from an enclosing function that has finished its execution.

There are three criteria for a Python closure:

\begin{itemize}
    \item Is a nested function (a function that is part of a parent
          function).
    \item Has access to \emph{free variable} from outer scope.
          A free variables is a variable that is not bound in the
          local scope (\verb!nonlocal! keyword).
    % This nested function has to refer to a variable defined
    % inside the enclosing function.
    \item The closure is returned from the enclosing function.
\end{itemize}

The general way to create a Python closure looks like this:

\begin{python}
def outer(*args):
    # This is the outer enclosing function
    # define variables

    # This is our closure:
    def inner():
        # This is the nested function
        # Use the nonlocal keyword here
    
    # we return the function(!) object, but NOT(!) call it:
    return inner
\end{python}

As a specific example to return consecutive numbers:

\begin{python}
def make_counter():
    count = 0

    def inner():
        nonlocal count
        count += 1
        return count

    return inner
\end{python}

It is possible to to avoid the \pyth{nonlocal} keyword and
store the result in the closure:

\begin{python}
def make_counter():
    def inner():
        inner.counter += 1
        return inner.counter

    # Set our default value:
    inner.counter = 0
    return inner
\end{python}

Use it as follows:

\begin{python}
>>> thecounter = make_counter()
>>> thecounter(); 
1
>>> thecounter.counter
1
\end{python}

It can also be used to \gquote{fix} a variable like in this
example:

\begin{python}
def make_multiplier(n):
    def multiplier(x):
        return x * n
    return multiplier

>>> times3 = make_multiplier(3)
>>> times3(3)
9
\end{python}

You can use closures:

\begin{itemize}
    \item to replace unnecessary small classes.
          In cases you have only one method, this is often
          more elegant to use a closure. 
    \item to help to reduce the use of global variables.
    \item to hide data.
    \item to remember a function environment even after 
          completes its execution.
\end{itemize}

% Decorators



\begin{thebibliography}{10}
\bibitem{burgaud.lambda}
    \bauthor{Andre Burgaud},
    \btitle{How to Use Python Lambda Functions},
    \url{https://realpython.com/python-lambda/}

\bibitem{google.styleguide}
   \bauthor{Google Inc.},
   \btitle{Comments and Docstrings},
   \url{https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings}

\bibitem{python.org}
    \bauthor{Python Org},
    \btitle{Python 3.x.0 documentation},
    \url{https://docs.python.org}

\bibitem{python.org.doctest}
    \bauthor{Python Org},
    \btitle{doctest},
    \url{https://docs.python.org/3/library/doctest.html}

\bibitem{python.org.func}
  \bauthor{Python Org},
  \btitle{Function definitions},
  \url{https://docs.python.org/3/reference/compound_stmts.html#function-definitions}

\bibitem{python.org.glossary}
  \bauthor{Python Org},
  \btitle{Glossary},
  \url{https://docs.python.org/3/glossary.html}

\bibitem{python.org.inspect}
  \bauthor{Python Org},
  \btitle{inspect},
  \url{https://docs.python.org/3/library/inspect.html?highlight=code#types-and-members}

\bibitem{python.org.typing}
  \bauthor{Python Org},
  \btitle{typing},
  \url{https://docs.python.org/3/library/typing.html}

\bibitem{hjelle.typechecking}
  \bauthor{Geir Arne Hjelle},
  \btitle{Python Type Checking (Guide)},
  \url{https://realpython.com/python-type-checking/}

\bibitem{diveinto}
    \bauthor{Mark Pilgrim},
    \btitle{Dive into Python3}
    \url{https://diveintopython3.problemsolving.io/}

\bibitem{hellman.pymotw}
    \bauthor{Doug Hellman},
    \btitle{Python 3 Module of the Week},
    \url{https://pymotw.com/3/}

\bibitem{numpydoc}
    \bauthor{Numpy},
    \btitle{numpydoc docstring guide},
    \url{https://numpydoc.readthedocs.io/en/latest/format.html}

\bibitem{ramalho}
    \bauthor{Luciano Ramalho},
    \btitle{Fluent Python},
    ISBN978-1-491-9-46008, O'{}Reilly

\bibitem{sphinx}
    \bauthor{Sphinx},
    \btitle{Python Documentation Generator}
    \url{https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html}
    % https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
\end{thebibliography}

\end{multicols}
\end{document}
