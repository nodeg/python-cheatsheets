\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

%opening
\title{Python3 Datatypes Cheatsheet}
% \titlehead{fooo}
\subtitle{Sequences: Lists, Tupels and Ranges}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
\label{sec.dict.definition}
A sequence is an \emph{Iterable} and contains zero or more objects.
An \emph{iterable} supports element-wise and efficient access via
integer indices.

There are three basic sequence types:

\begin{itemize}
\item \pyth{list} are mutable sequences (\gquote{arrays}).
\item \pyth{tuple} are immutable sequences (\gquote{Arrays}).
\item \pyth{range} are immutable sequences of numbers usually
used in loops.
\end{itemize}

In addition, there are sequence types (not explained here)
that are specifically designed for binary data and strings:

\begin{itemize}
\item \pyth{str} (strings) are immutable sequences of Unicode 
codepoints.
\item \pyth{bytearray} are mutable sequences of integers
in the range $0 <= x < 256$.
\item \pyth{bytes} like \pyth{bytearray}, however \pyth{bytes}
are immutable sequences.
\item \pyth{memoryview} allows to access the internal data of
an object which supports the buffer protocol without copying data.
\end{itemize}


\section{Creating Lists}
Lists can be created in the following ways:

\begin{itemize}
\item asn an empty list with a pair of brackets: \pyth{[]}
\item as a pair of square brackets and with commas to separate individual entries:
    \pyth{[1, 2, 3]}
\item as a list comprehension: \pyth{[x for x in iterable]}
\item with the \pyth{list} constructor: \pyth{list()} or
    \pyth{list(iterable)}
\end{itemize}


\section{Creating Tuples}
Tuples can be created in the following ways:

\begin{itemize}
\item as empty tuple with a round parenthesis:
    \pyth{()}
\item as a tuple with an entry, where the entry contains a trailing comma:
    \pyth{(1,)}
\item as a pair of parenthesis and with commas to separate individual entries:
    \pyth{(1, 2, 3)} oder \pyth{1, 2, 3}
\item with the \pyth{tuple} constructor: \pyth{tuple()} or
    \pyth{tuple(iterable)}
\end{itemize}


\section{Creating \pyth{range} Objects}
Syntax: \pyth{range(start, stop [, step])}

A \pyth{range} object is a sequence of integers.
Starting from \pyth{start} (inclusive) to \pyth{stop}
(exclusive) with the distance \pyth{step}.
If \pyth{start} is omitted, the range is counted from zero:

\begin{python}
>>> list(range(6))
[0, 1, 2, 3, 4, 5]
>>> list(range(1, 6))
[1, 2, 3, 4, 5]
>>> list(range(1, 6, 2))
[1, 3, 5]
>>> list(range(1, -6, -2))
[1, -1, -3, -5]
\end{python}

\section{Common Operations}

\begin{itemize}
\item \pyth{x in seq}: is item x contained in sequence seq?
\item \pyth{x not in seq}: is item x \emph{not} contained in sequence seq?
\item \pyth{seq + t}: concat sequences seq and t
\item \pyth{seq * n} or \pyth{n * seq}: create copies of the sequence seq n times
\item \pyth{seq[i]}: i-th item of the sequence seq (indexing);
    counting starts at zero, negative indices are allowed and count from the end.
    If the index is greater than the length of the sequence,
    \pyth{IndexError} is raised.
\item \pyth{seq[i:j]}: slice sequence seq, starting from the i-th
    item to the j-th item (not included)
\item \pyth{seq[i:j:k]}: slice sequence seq like \pyth{seq[i:j]}, but with step k
\item \pyth{len(seq)}: number of items within seq
\item \pyth{min(seq)}: smallest item of the sequence seq
\item \pyth{max(seq)}: largest item of the sequence seq
\end{itemize}


\section{Walking through Sequences}
Each sequence can be walked through item by item (\gquote{iteration}).
The general syntax is:

\begin{python}
>>> for item in sequence:
...    # Do something with item
\end{python}

If the position of the item is required, use the built-in
function \pyth{enumerate(iterable[, start])}:

\begin{python}
>>> for index, item in enumerate(sequence):
...    # Do something with index and item
\end{python}


\section{\pyth{slice}}
Syntax: \pyth{slice(stop)} or \pyth{slice(start, stop[, step])}

A \pyth{slice} object is used to create a portion of a sequence.
It can be stored in a variable and passed in the subscript notation:

\begin{python}
>>> s1 = slice(3)
>>> s1
slice(None, 3, None)
>>> s2 = slice(2, 10, 2)
>>> l = list(range(10))
>>> l[s1]
[0, 1, 2]
>>> l[s2]
[2, 4, 6, 8]
\end{python}

\section{List Operations}

\subsection{Accessing Items of a List}
Each item of a list is accessible via an index.
The first element gets the index zero:

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist[0]
'one'
>>> mylist[2]
'three'
\end{python}

Negative numbers count from behind:

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist[-1]
'three'
>>> mylist[-2]
'two'
\end{python}

\subsection{Inserting Items}

\subsubsection{Appending to End}
Syntax: \pyth{listvar.append(object)}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist.append('four')
# Result: ['one', 'two', 'three', 'four']
\end{python}

\subsubsection{Inserting at a Specific Position}
Syntax: \pyth{listvar.insert(index, object)}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist.insert(1, 'four')
# Result: ['one', 'four', 'two', 'three']
\end{python}

\subsubsection{Extending via another List}
Syntax: \pyth{listvar.extend(iterable)}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> list2 = ['four']
>>> mylist.extend(list2)
# Result: ['one', 'two', 'three', 'four']
\end{python}


\subsection{Changing a Specific Item}
Syntax: \pyth{listvar[index] = object}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist[1] = 'five'
>>> mylist
['one', 'five', 'three']
\end{python}


\subsection{Reversing a List}
The method \pyth{.reverse()} is used to modify a list \iqq{in-place}:

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist.reverse()  # <- kein Rueckgabewert!
\end{python}

In contrast, the built-in function \pyth{reversed()} returns an iterator:

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> list(reversed(mylist))
['three', 'two', 'one']
\end{python}

\subsection{Deleting Items from a List}

\subsubsection{Via the Index}
Syntax: \pyth{listvar.pop([index])}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist.pop(2)
'three'
>>> mylist
['one', 'two']
\end{python}

Without the index, the last item is returned and deleted:

\begin{python}
>>> mylist.pop()
'two'
>>> mylist
['one']
\end{python}

\subsubsection{Via the Value of the Item}
Syntax: \pyth{listvar.remove(value)}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> mylist.remove('two') # kein Rueckgabewert!
>>> mylist
['one', 'three']
\end{python}

\subsubsection{Via the Position with \pyth{del()}}
Syntax: \pyth{del listvar[index]}

\begin{python}
>>> mylist = ['one', 'two', 'three']
>>> del mylist[2]  # no return value!
>>> mylist
['one', 'two']
\end{python}

The function \pyth{del()} can also use slices to delete
several items simultaneously:

\begin{python}
>>> mylist = ['one', 'two', 'three', 'four',
...           'five']
>>> del mylist[1:3]
>>> mylist
['one', 'four', 'five']
\end{python}


\subsection{Comparing Lists}

\begin{python}
>>> l1 = ['one', 'two', 'three']
>>> l2 = ['one', 'two', 'three']
>>> l3 = ['five', 'two', 'three']
>>> l1 == l2
True
>>> l1 == l3
False
\end{python}

\subsection{Sorting}
Of the three basic types \pyth{list}, \pyth{tuple} and
\pyth{range} only \pyth{list} can be sorted.

\subsubsection{Sorting In-Place}
The \pyth{list.sort()} method sorts the list in-place:

\begin{python}
>>> l = [-10, 100, 5, -2, 4]
>>> l.sort() # <- kein Rueckgabewert!
>>> l
[-10, -2, 4, 5, 100]
\end{python}

\subsubsection{Sorting with \pyth{sorted}}
The built-in function \pyth{sorted} does not change the
itable, but returns a sorted one:

\begin{python}
>>> l = [-10, 100, 5, -2, 4]
>>> sorted(l)
[-10, -2, 4, 5, 100]
\end{python}

Using the argument \pyth{reverse=True} the list can also
be output in reverse order:

\begin{python}
>>> sorted(l, reverse=True)
[100, 5, 4, -2, -10]
\end{python}

For a custom sort function, pass the keyword
\pyth{key} to \pyth{sorted}:

\begin{python}
>>> fruits = ['Oranges', 'Apples', 'Kiwis']
>>> sorted(fruits, key=str.lower)
['Apples', 'Kiwis', 'Oranges']
\end{python}

For nested list structures the \module{operator} module is useful.
In the following example, \pyth{itemgetter} returns the item
at the respective passed index position:

\begin{python}
>>> from operator import itemgetter
>>> students = [
        # Name (=0), age (=1), grade (=2)
...     ('Dave',     21,       'B'),
...     ('Tux',      23,       'A'),
...    ]
>>> # Sort by age:
>>> sorted(students, key=itemgetter(1))
[('Dave', 21, 'B'),('Tux', 23, 'A')]
>>> # Sort by grade:
>>> sorted(students, key=itemgetter(2))
[('Tux', 23, 'A'), ('Dave', 21, 'B')]
\end{python}


\end{multicols}
\end{document}
